﻿using RPSE.Shared.Auth;
using RPSE.Shared.Conteners;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading.Tasks;
using RPSE.Client.Libs.Proxy;

namespace RPSE.Client.Libs
{
    public class TcpClient
    {
        public const int BufferSize = 8096;

        public GameServiceProxy GameServiceProxy { get; set; }
        public AccountServiceProxy AccountServiceProxy { get; set; }
        string ip;
        private Socket server;

        public TcpClient(string ip)
        {
            this.ip = ip;
        }

        public byte[] Compress(object o)
        {
            byte[] bytes;
            IFormatter formatter = new BinaryFormatter();
            using (MemoryStream stream = new MemoryStream())
            {
                formatter.Serialize(stream, o);
                bytes = stream.ToArray();
            }

            return bytes;
        }

        private Object Decompress(byte[] bytes)
        {
            object o;
            IFormatter formatter = new BinaryFormatter();
            using (MemoryStream stream = new MemoryStream(bytes))
            {
                o = formatter.Deserialize(stream);
            }

            return o;
        }

        public void Init()
        {
            server = new Socket(AddressFamily.InterNetwork,
                SocketType.Stream, ProtocolType.Tcp);

            server.Connect(IPAddress.Parse(ip), 6969);

            //Socket client = tcp.AcceptSocket();
            Console.WriteLine("C: Connection accepted. :)");

            Task.Run(() =>
            {
                byte[] data = new byte[BufferSize];

                while (true)
                {

                    int size = server.Receive(data);

                    var trimData = new byte[size];
                    Array.Copy(data, trimData, trimData.Length);
                    Recieve(trimData);

                    Console.WriteLine("C: Recieved data: ");
                    /*
                    for (int i = 0; i < size; i++)
                        Console.Write(Convert.ToChar(data[i]));
                    Console.WriteLine();*/
                }
            });
        }

        private void Recieve(byte[] bytes)
        {
            byte[][] deserialized = (byte[][])Decompress(bytes);
            string function = (string)Decompress(deserialized[0]);


            Console.WriteLine(function + " called");
            switch (function)
            {
                case "RunOfferCommand":
                    int id = (int)Decompress(deserialized[1]);
                    OfferToken offerToken = (OfferToken)Decompress(deserialized[2]);
                    OfferInfo offerInfo = (OfferInfo)Decompress(deserialized[3]);

                    GameServiceProxy
                        .GameOfferCommands
                        .SingleOrDefault(x => x.Key == id)
                        .Value
                        .Execute(offerToken, offerInfo);
                    break;
                case "RunGameOfferRejectedCommand":
                    int id1 = (int)Decompress(deserialized[1]);
                    string username = (string)Decompress(deserialized[2]);

                    GameServiceProxy
                        .GameOfferRejectedCommands
                        .SingleOrDefault(x => x.Key == id1)
                        .Value
                        .Execute(username);
                    break;
                case "RunGameEstablishedCommand":
                    int id2 = (int)Decompress(deserialized[1]);
                    GameToken gameToken = (GameToken)Decompress(deserialized[2]);

                    GameServiceProxy
                        .GameEstablishedCommands
                        .SingleOrDefault(x => x.Key == id2)
                        .Value
                        .Execute(gameToken);
                    break;
                case "RunProxyRoundStartCommand":
                    int id3 = (int)Decompress(deserialized[1]);
                    RoundInfo roundInfo = (RoundInfo)Decompress(deserialized[2]);

                    GameServiceProxy
                        .RoundStartCommands
                        .SingleOrDefault(x => x.Key == id3)
                        .Value
                        .Execute(roundInfo);
                    break;
                case "RunProxyRoundOverCommand":
                    int id4 = (int)Decompress(deserialized[1]);
                    RoundOverInfo roundInfo1 = (RoundOverInfo)Decompress(deserialized[2]);

                    GameServiceProxy
                        .RoundOverCommands
                        .SingleOrDefault(x => x.Key == id4)
                        .Value
                        .Execute(roundInfo1);
                    break;
                case "RunGameOverCommand":
                    int id5 = (int)Decompress(deserialized[1]);
                    GameOverInfo gameOverInfo = (GameOverInfo)Decompress(deserialized[2]);

                    GameServiceProxy
                        .GameOverCommands
                        .SingleOrDefault(x => x.Key == id5)
                        .Value
                        .Execute(gameOverInfo);
                    break;
                case "LoginResult":
                    string state = (string)Decompress(deserialized[1]);
                    UserToken userToken_ = null;

                    if (state != "null")
                    {
                        userToken_ = (UserToken)Decompress(deserialized[2]);
                    }

                    AccountServiceProxy.LoginResult = userToken_;
                    AccountServiceProxy.CallBackDone.Set();

                    break;
                case "RegisterResult":
                    bool result = (bool)Decompress(deserialized[1]);

                    AccountServiceProxy.RegisterResult = result;
                    AccountServiceProxy.CallBackDone.Set();
                    break;
                case "GetReadyUsersResult":
                    IList<string> result_ = (IList<string>)Decompress(deserialized[1]);

                    GameServiceProxy.GetReadyUsersResult = result_;
                    GameServiceProxy.CallBackDone.Set();
                    
                    break;
                default:
                    Console.WriteLine("Default case");
                    break;
            }
        }

        public void Send(byte[] bytes)
        {
            server.Send(bytes);
        }

    }
}
