﻿using System.Collections.Generic;
using RPSE.Shared.Auth;
using RPSE.Shared.Commands;
using RPSE.Shared.Interfaces;
using RPSE.Shared.Items;

namespace RPSE.Client.Libs.Adapter
{//WZORZEC ADAPTER
    public class GameAdapter
    {
        private IGameService _gameService;

        public GameAdapter(IGameService gameService)
        {
            _gameService = gameService;
        }

        public UserToken UserToken { get; set; }

        public void AcceptOffer(OfferToken offerToken, GameEstablishedCommand establishedCommand)
        {
            _gameService.AcceptOffer(UserToken, offerToken, establishedCommand);
        }

        public void DontWaitToGame()
        {
            _gameService.DontWaitToGame(UserToken);
        }

        public IList<string> GetReadyUsers()
        {
            return _gameService.GetReadyUsers();
        }

        public void InitGame(GameToken gameToken, RoundStartCommand roundStartCommand, RoundOverCommand roundOverCommand, GameOverCommand gameOverCommand)
        {
            _gameService.InitGame(UserToken, gameToken, roundStartCommand, roundOverCommand, gameOverCommand);
        }

        public void PlayWithUser(string username, GameEstablishedCommand establishedCommand, GameOfferRejectedCommand rejectedCommand)
        {
            _gameService.PlayWithUser(UserToken, username, establishedCommand, rejectedCommand);
        }

        public void RejectOffer(OfferToken offerToken)
        {
            _gameService.RejectOffer(UserToken, offerToken);
        }

        public void SelectItem(GameToken gameToken, Item item)
        {
            _gameService.SelectItem(UserToken, gameToken, item);
        }

        public void Surrender(GameToken gameToken)
        {
            _gameService.Surrender(UserToken, gameToken);
        }

        public void WaitToGame(GameOfferCommand command)
        {
            _gameService.WaitToGame(UserToken, command);
        }
    }
}
