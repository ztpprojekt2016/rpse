﻿using System;
using System.IO;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System.Threading;
using RPSE.Shared.Auth;
using RPSE.Shared.Interfaces;

namespace RPSE.Client.Libs.Proxy
{//WZORZEC PROXY
    public class AccountServiceProxy : IAccountService
    {
        public TcpClient TcpClient { get; set; }

        public ManualResetEvent CallBackDone { get; set; } =
            new ManualResetEvent(false);

        public UserToken LoginResult { get; set; }
        public bool RegisterResult { get; set; }

        private byte[] Compress(object o)
        {
            byte[] bytes;
            IFormatter formatter = new BinaryFormatter();
            using (MemoryStream stream = new MemoryStream())
            {
                formatter.Serialize(stream, o);
                bytes = stream.ToArray();
            }

            return bytes;
        }

        public UserToken Login(string username, string password)
        {
            byte[][] message = new byte[3][];
            message[0] = Compress("Login");
            message[1] = Compress(username);
            message[2] = Compress(password);

            byte[] bytes = Compress(message);
            TcpClient.Send(bytes);

            CallBackDone.WaitOne();
            CallBackDone.Reset();

            return LoginResult;
        }

        public void Logout(UserToken userToken)
        {
            byte[][] message = new byte[2][];
            message[0] = Compress("Logout");
            message[1] = Compress(userToken);

            byte[] bytes = Compress(message);
            TcpClient.Send(bytes);
        }

        public bool Register(string username, string password)
        {
            byte[][] message = new byte[3][];
            message[0] = Compress("Register");
            message[1] = Compress(username);
            message[2] = Compress(password);

            byte[] bytes = Compress(message);
            TcpClient.Send(bytes);

            CallBackDone.WaitOne();
            CallBackDone.Reset();

            return RegisterResult;
        }
    }
}
