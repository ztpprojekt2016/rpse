﻿using System.Collections.Generic;
using RPSE.Shared.Auth;
using RPSE.Shared.Commands;
using RPSE.Shared.Interfaces;
using RPSE.Shared.Items;
using System.Net.Sockets;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;
using System.Linq;
using System.Text;
using System;
using System.Net;
using System.Threading;
using System.Threading.Tasks;

namespace RPSE.Client.Libs
{
    public class GameServiceProxy : IGameService
    {
        public TcpClient TcpClient { get; set; }
        
        public Dictionary<int, GameOfferCommand> GameOfferCommands { get; set; } = new Dictionary<int, GameOfferCommand>();
        public Dictionary<int, GameOfferRejectedCommand> GameOfferRejectedCommands { get; set; } = new Dictionary<int, GameOfferRejectedCommand>();
        public Dictionary<int, GameEstablishedCommand> GameEstablishedCommands { get; set; } = new Dictionary<int, GameEstablishedCommand>();
        public Dictionary<int, RoundStartCommand> RoundStartCommands { get; set; } = new Dictionary<int, RoundStartCommand>();
        public Dictionary<int, RoundOverCommand> RoundOverCommands { get; set; } = new Dictionary<int, RoundOverCommand>();
        public Dictionary<int, GameOverCommand> GameOverCommands { get; set; } = new Dictionary<int, GameOverCommand>();
        public GameServiceProxy()
        {
            //connection = Connection.StartClient();
        }


        public byte[] Compress(object o)
        {
            byte[] bytes;
            IFormatter formatter = new BinaryFormatter();
            using (MemoryStream stream = new MemoryStream())
            {
                formatter.Serialize(stream, o);
                bytes = stream.ToArray();
            }

            return bytes;
        }



        public ManualResetEvent CallBackDone { get; set; } =
            new ManualResetEvent(false);
        public IList<string> GetReadyUsersResult { get; set; }
        public IList<string> GetReadyUsers()
        {
            byte[][] message = new byte[3][];
            message[0] = Compress("GetReadyUsers");

            byte[] bytes = Compress(message);
            TcpClient.Send(bytes);

            CallBackDone.WaitOne();
            CallBackDone.Reset();

            return GetReadyUsersResult;
        }


        public void WaitToGame(UserToken userToken, GameOfferCommand command)
        {
            int id = 1;
            if (GameOfferCommands.Any())
                id = GameOfferCommands.Keys.Max() + 1;

            GameOfferCommands.Add(id, command);

            byte[][] message = new byte[3][];
            message[0] = Compress("WaitToGame");
            message[1] = Compress(id);
            message[2] = Compress(userToken);

            byte[] bytes = Compress(message);
            //Console.WriteLine(bytes.Length);
            TcpClient.Send(bytes);
            //gameService.WaitToGame(userToken, command);
        }

        public void DontWaitToGame(UserToken userToken)
        {
            byte[][] message = new byte[2][];
            message[0] = Compress("DontWaitToGame");
            message[1] = Compress(userToken);

            byte[] bytes = Compress(message);
            TcpClient.Send(bytes);
            //gameService.DontWaitToGame(userToken);
        }

        public void PlayWithUser(UserToken userToken, string username, GameEstablishedCommand establishedCommand,
            GameOfferRejectedCommand rejectedCommand)
        {
            int idest = 1;
            if (GameEstablishedCommands.Any())
                idest = GameEstablishedCommands.Keys.Max() + 1;

            int idre = 1;
            if (GameOfferRejectedCommands.Any())
                idre = GameOfferRejectedCommands.Keys.Max() + 1;

            GameOfferRejectedCommands.Add(idre, rejectedCommand);
            GameEstablishedCommands.Add(idest, establishedCommand);

            byte[][] message = new byte[5][];
            message[0] = Compress("PlayWithUser");
            message[1] = Compress(idest);
            message[2] = Compress(idre);
            message[3] = Compress(userToken);
            message[4] = Compress(username);

            byte[] bytes = Compress(message);
            //Console.WriteLine(bytes.Length);
            TcpClient.Send(bytes);
            //gameService.PlayWithUser(userToken, username, establishedCommand, rejectedCommand);
        }

        public void AcceptOffer(UserToken userToken, OfferToken offerToken, GameEstablishedCommand establishedCommand)
        {
            int id = 1;
            if (GameEstablishedCommands.Any())
                id = GameEstablishedCommands.Keys.Max() + 1;

            GameEstablishedCommands.Add(id, establishedCommand);

            byte[][] message = new byte[4][];
            message[0] = Compress("AcceptOffer");
            message[1] = Compress(id);
            message[2] = Compress(userToken);
            message[3] = Compress(offerToken);

            byte[] bytes = Compress(message);
            //Console.WriteLine(bytes.Length);
            TcpClient.Send(bytes);
            //gameService.AcceptOffer(userToken, offerToken, establishedCommand);
        }

        public void RejectOffer(UserToken userToken, OfferToken offerToken)
        {
            byte[][] message = new byte[3][];
            message[0] = Compress("RejectOffer");
            message[1] = Compress(userToken);
            message[2] = Compress(offerToken);

            byte[] bytes = Compress(message);
            TcpClient.Send(bytes);
            //gameService.RejectOffer(userToken, offerToken);
        }

        public void InitGame(UserToken userToken, GameToken gameToken, RoundStartCommand roundStartCommand,
            RoundOverCommand roundOverCommand, GameOverCommand gameOverCommand)
        {
            int idst = 1;
            if (RoundStartCommands.Any())
                idst = RoundStartCommands.Keys.Max() + 1;

            int idro = 1;
            if (RoundOverCommands.Any())
                idro = RoundOverCommands.Keys.Max() + 1;

            int idgo = 1;
            if (GameOverCommands.Any())
                idgo = GameOverCommands.Keys.Max() + 1;

            RoundStartCommands.Add(idst, roundStartCommand);
            RoundOverCommands.Add(idro, roundOverCommand);
            GameOverCommands.Add(idgo, gameOverCommand);

            byte[][] message = new byte[6][];
            message[0] = Compress("InitGame");
            message[1] = Compress(idst);
            message[2] = Compress(idro);
            message[3] = Compress(idgo);
            message[4] = Compress(userToken);
            message[5] = Compress(gameToken);

            byte[] bytes = Compress(message);
            //Console.WriteLine(bytes.Length);
            TcpClient.Send(bytes);

            //gameService.InitGame(userToken, gameToken, roundStartCommand, roundOverCommand, gameOverCommand);
        }

        /*public void ReadyToRound(UserToken userToken, GameToken gameToken)
        {
            gameService.ReadyToRound(userToken, gameToken);
        }*/

        public void SelectItem(UserToken userToken, GameToken gameToken, Item item)
        {
            byte[][] message = new byte[4][];
            message[0] = Compress("SelectItem");
            message[1] = Compress(userToken);
            message[2] = Compress(gameToken);
            message[3] = Compress(item);

            byte[] bytes = Compress(message);
            TcpClient.Send(bytes);
            //gameService.SelectItem(userToken, gameToken, item);
        }

        public void Surrender(UserToken userToken, GameToken gameToken)
        {
            byte[][] message = new byte[3][];
            message[0] = Compress("Surrender");
            message[1] = Compress(userToken);
            message[2] = Compress(gameToken);

            byte[] bytes = Compress(message);
            TcpClient.Send(bytes);
            //gameService.Surrender(userToken, gameToken);
        }
    }
}
