﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RPSE.Shared.Auth;

namespace RPSE.Shared.Interfaces
{
    public interface IAccountService
    {
        bool Register(string username, string password);
        UserToken Login(string username, string password);
        void Logout(UserToken userToken);
    }
}
