﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Media;
using System.Text;
using System.Threading.Tasks;
using RPSE.Shared.Auth;
using RPSE.Shared.Commands;
using RPSE.Shared.Items;

namespace RPSE.Shared.Interfaces
{
    public interface IGameService
    {
        IList<string> GetReadyUsers(); //todo make custom object
        void WaitToGame(UserToken userToken, GameOfferCommand command);
        void DontWaitToGame(UserToken userToken);

        void PlayWithUser(UserToken userToken, string username, GameEstablishedCommand establishedCommand,
            GameOfferRejectedCommand rejectedCommand);

        void AcceptOffer(UserToken userToken, OfferToken offerToken, GameEstablishedCommand establishedCommand);
        void RejectOffer(UserToken userToken, OfferToken offerToken);

        void InitGame(UserToken userToken, GameToken gameToken, RoundStartCommand roundStartCommand,
            RoundOverCommand roundOverCommand, GameOverCommand gameOverCommand);

        //void ReadyToRound(UserToken userToken, GameToken gameToken);

        void SelectItem(UserToken userToken, GameToken gameToken, Item item);
        void Surrender(UserToken userToken, GameToken gameToken);
    }
}
