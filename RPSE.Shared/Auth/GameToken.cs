﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;

namespace RPSE.Shared.Auth
{
    [Serializable]
    public class GameToken : TokenBase
    {
        public GameToken(string token) : base(token)
        {
        }
    }
}
