﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPSE.Shared.Auth
{
    [Serializable]
    public abstract class TokenBase
    {
        public string Token { get; private set; }

        public TokenBase(string token)
        {
            Token = token;
        }

        public override bool Equals(object obj)
        {
            var userToken = obj as TokenBase;

            if (userToken == null)
                return false;

            if (userToken.Token != this.Token)
                return false;

            return true;
        }

        public override int GetHashCode()
        {
            int hash = 13;
            hash = (hash * 7) + Token.GetHashCode();

            return hash;
        }
    }
}
