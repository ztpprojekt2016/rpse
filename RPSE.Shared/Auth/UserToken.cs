﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPSE.Shared.Auth
{

    [Serializable]
    public class UserToken : TokenBase
    {
        public UserToken(string username, string token) : base(token)
        {
            Username = username;
        }

        public string Username { get; protected set; }

        public override bool Equals(object obj)
        {
            if (!base.Equals(obj))
                return false;

            var userToken = obj as UserToken;

            if (userToken == null)
                return false;

            if (userToken.Username != this.Username)
                return false;

            return true;
        }

        public override int GetHashCode()
        {
            int hash = base.GetHashCode();
            hash = (hash * 7) + Username.GetHashCode();

            return hash;
        }
    }
}
