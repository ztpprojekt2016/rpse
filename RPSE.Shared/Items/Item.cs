﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RPSE.Shared.Enums;

namespace RPSE.Shared.Items
{
    [Serializable]
    public abstract class Item : ICloneable
    {
        protected abstract IList<string> BeatsId { get; }
        protected abstract string Id { get; }
        public abstract GameTypeEnum Type { get; }

        public string Name { get; protected set; }
        public string ImageUrl { get; protected set; }

        public bool TryWin(Item item)
        {
            if (this.BeatsId.Contains(item.Id))
                return true;

            return false;
        }

        public object Clone()
        {
            return this.MemberwiseClone();
        }
    }
}
