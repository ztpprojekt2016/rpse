﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading.Tasks;
using RPSE.Shared.Enums;

namespace RPSE.Shared.Items
{
    [Serializable]
    public class ProxyItem : Item
    {
        private byte[] serializedItem;

        public ProxyItem(Item item)
        {
            Type = item.Type;
            Name = item.Name;
            ImageUrl = item.ImageUrl;

            IFormatter formatter = new BinaryFormatter();
            using (MemoryStream stream = new MemoryStream())
            {
                formatter.Serialize(stream, item);
                serializedItem = stream.ToArray();
            }
        }

        public Item GetItem()
        {
            Item o;
            IFormatter formatter = new BinaryFormatter();
            using (MemoryStream stream = new MemoryStream(serializedItem))
            {
                o = (Item)formatter.Deserialize(stream);
            }

            return o;
        }

        protected override IList<string> BeatsId { get; }
        protected override string Id { get; }
        public override GameTypeEnum Type { get; }
    }
}
