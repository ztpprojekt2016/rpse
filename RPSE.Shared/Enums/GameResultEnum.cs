﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPSE.Shared.Enums
{
    public enum GameResultEnum
    {
        None = 0,
        Won,
        Lost,
        Draw
    }
}
