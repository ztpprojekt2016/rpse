﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RPSE.Shared.Enums;
using RPSE.Shared.Items;

namespace RPSE.Shared.Conteners
{
    [Serializable]
    public class RoundOverInfo
    {
        public int Points { get; set; }
        public GameResultEnum Result { get; set; }
        public Dictionary<string, Item> Choices { get; set; }
    }
}
