﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RPSE.Shared.Items;

namespace RPSE.Shared.Conteners
{
    [Serializable]
    public class RoundInfo
    {
        public Item[] Items { get; set; }
        public int Round { get; set; }

        public string Player { get; set; }
        public IList<string> Users { get; set; }
        
    }
}
