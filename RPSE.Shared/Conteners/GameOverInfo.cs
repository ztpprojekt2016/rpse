﻿using System.Collections.Generic;
using RPSE.Shared.Enums;
using System;

namespace RPSE.Shared.Conteners
{
    [Serializable]
    public class GameOverInfo
    {
        public int Points { get; set; }
        public GameResultEnum Result { get; set; }   
    }
}
