﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RPSE.Shared.Auth;

namespace RPSE.Shared.Commands
{
    [Serializable]
    public abstract class GameEstablishedCommand
    {
        public abstract void Execute(GameToken gameToken);
    }
}
