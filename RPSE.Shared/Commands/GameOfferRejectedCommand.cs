﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPSE.Shared.Commands
{
    [Serializable]
    public abstract class GameOfferRejectedCommand
    {
        public abstract void Execute(string username);
    }
}
