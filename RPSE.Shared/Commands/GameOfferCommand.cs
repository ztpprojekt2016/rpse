﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RPSE.Shared.Auth;
using RPSE.Shared.Conteners;

namespace RPSE.Shared.Commands
{
    [Serializable]
    public abstract class GameOfferCommand
    {
        public abstract void Execute(OfferToken offerToken, OfferInfo offerInfo);
    }
}
