﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using RPSE.Server.Core.Exceptions;
using RPSE.Server.Core.Extensions;
using RPSE.Server.Core.Game;
using RPSE.Server.Core.Implementation;
using RPSE.Server.Core.Implementation.GameOffers;
using RPSE.Server.Core.Implementation.Games;
using RPSE.Server.Core.Implementation.Tokens;
using RPSE.Server.Core.Implementation.Users;
using RPSE.Shared;
using RPSE.Shared.Auth;
using RPSE.Shared.Commands;
using RPSE.Shared.Conteners;
using RPSE.Shared.Interfaces;
using RPSE.Shared.Items;

namespace RPSE.Server.Core.Services
{
    public class GameService : IGameService //uwaga to powinien być singleton, ale nie deklarujemy tego
    {
        public IList<string> GetReadyUsers()
        {
            var db = UserBaseSingleton.GetInstance();

            List<string> users = db.GetOnlineUsers()
                .Where(x => x.GameOfferCommand != null)
                .Select(x => x.Username)
                .ToList();

            return users;
        }

        public void WaitToGame(UserToken userToken, GameOfferCommand command)
        {
            if (userToken == null)
                throw new ArgumentNullException(nameof(userToken));

            if (command == null)
                throw new ArgumentNullException(nameof(command));


            var db = UserBaseSingleton.GetInstance();

            var user = db.GetUser(userToken);

            if (user == null)
                throw new UserNotLoggedException(userToken.Username);

            user.GameOfferCommand = command;
            //todo save
        }

        public void DontWaitToGame(UserToken userToken)
        {
            if (userToken == null)
                throw new ArgumentNullException(nameof(userToken));

            var db = UserBaseSingleton.GetInstance();

            var user = db.GetUser(userToken);

            if (user == null)
                throw new UserNotLoggedException(userToken.Username);

            //user.Online = false;
            user.GameOfferCommand = null;

            //todo save
        }

        public void PlayWithUser(UserToken userToken, string username, GameEstablishedCommand establishedCommand,
            GameOfferRejectedCommand rejectedCommand)
        {
            if (userToken == null)
                throw new ArgumentNullException(nameof(userToken));

            if (username == null)
                throw new ArgumentNullException(nameof(username));

            if (establishedCommand == null)
                throw new ArgumentNullException(nameof(establishedCommand));
            
            if (rejectedCommand == null)
                throw new ArgumentNullException(nameof(rejectedCommand));
            
            var db = UserBaseSingleton.GetInstance();

            var user = db.GetUser(userToken);
            
            
            if (user == null)
                throw new UserNotLoggedException(userToken.Username);
            
            var opponent = db.GetUser(username);

            if (opponent == null)
                throw new UserNotExistException(username);
            
            if (opponent.Username == user.Username)
                throw new PlayWithYourselfException(opponent.Username);

            if (opponent.Online == false || opponent.UserToken == null) //fixme wyścig!
            {
                Task.Run(() =>
                {
                    rejectedCommand.Execute(opponent.Username);//todo powód odrzucenia..
                });

                return;
            }

            var factory = new TokenFactory();

            var offer = new GameOffer();
            offer.User1 = user;
            offer.User2 = opponent;
            offer.OfferToken = factory.CreateOfferToken();
            offer.OnAccepted = establishedCommand;
            offer.OnRejected = rejectedCommand;

            GameOfferSingleton.GetInstance()
                .Add(offer);

            Task.Run(() =>
            {
                opponent.GameOfferCommand.Execute(offer.OfferToken, new OfferInfo());//fixme
            });
        }

        public void AcceptOffer(UserToken userToken, OfferToken offerToken, GameEstablishedCommand establishedCommand)
        {
            if (userToken == null)
                throw new ArgumentNullException(nameof(userToken));

            if (offerToken == null)
                throw new ArgumentNullException(nameof(offerToken));

            if (establishedCommand == null)
                throw new ArgumentNullException(nameof(establishedCommand));

            var db = UserBaseSingleton.GetInstance();

            var user = db.GetUser(userToken);

            if (user == null)
                throw new UserNotLoggedException(userToken.Username);

            var offerdb = GameOfferSingleton.GetInstance();
            var offer = offerdb.GetByOfferToken(offerToken);


            if (user != offer.User2)
                return;

            var gamedb = GameBaseSingleton.GetInstance();

            var token = new TokenFactory().CreateGameToken();
            var game = new Implementation.Games.Game();

            game.GameToken = token;
            game.AddUserToGame(offer.User1);
            game.AddUserToGame(offer.User2);
            gamedb.Add(game);

            Task.Run(() =>
            {
                establishedCommand.Execute(token);
            });

            Task.Run(() =>
            {
                offer.OnAccepted.Execute(token);
            });

            offerdb.Remove(offer);
        }

        public void RejectOffer(UserToken userToken, OfferToken offerToken)
        {
            if (userToken == null)
                throw new ArgumentNullException(nameof(userToken));

            if (offerToken == null)
                throw new ArgumentNullException(nameof(offerToken));

            var db = UserBaseSingleton.GetInstance();

            var user = db.GetUser(userToken);

            if (user == null)
                throw new UserNotLoggedException(userToken.Username);

            var offerdb = GameOfferSingleton.GetInstance();
            var offer = offerdb.GetByOfferToken(offerToken);

            Task.Run(() =>
            {
                offer.OnRejected.Execute(user.Username);
            });

            offerdb.Remove(offer);
        }

        private object initGameLock = new object();
        public void InitGame(UserToken userToken, GameToken gameToken, RoundStartCommand roundStartCommand,
            RoundOverCommand roundOverCommand, GameOverCommand gameOverCommand)
        {
            if (userToken == null)
                throw new ArgumentNullException(nameof(userToken));

            if (gameToken == null)
                throw new ArgumentNullException(nameof(gameToken));

            if (roundStartCommand == null)
                throw new ArgumentNullException(nameof(roundStartCommand));

            if (roundOverCommand == null)
                throw new ArgumentNullException(nameof(roundOverCommand));

            if (gameOverCommand == null)
                throw new ArgumentNullException(nameof(gameOverCommand));

            var dbGame = GameBaseSingleton.GetInstance();
            var dbUser = UserBaseSingleton.GetInstance();

            var user = dbUser.GetUser(userToken);
            var game = dbGame.Get(gameToken);

            if (user == null)
                throw new UserNotLoggedException(userToken.Username);


            bool inited = game.Init(user, gameOverCommand, roundOverCommand, roundStartCommand);
            bool stop = false;

            if (!inited)
            {
                lock (initGameLock)
                {
                    if (!inited)
                    {
                        stop = true;
                    }
                }
            }
            
            if(stop)
                return;

            Task.Run(() =>
            {
                game.Start();
            });

        }

        public void SelectItem(UserToken userToken, GameToken gameToken, Item item)
        {
            if (userToken == null)
                throw new ArgumentNullException(nameof(userToken));

            if (gameToken == null)
                throw new ArgumentNullException(nameof(gameToken));

            if (item == null)
                throw new ArgumentNullException(nameof(item));

            var dbGame = GameBaseSingleton.GetInstance();
            var dbUser = UserBaseSingleton.GetInstance();

            var user = dbUser.GetUser(userToken);
            var game = dbGame.Get(gameToken);

            if (user == null)
                throw new UserNotLoggedException(userToken.Username);

            game.SelectItem(user, item);
        }

        public void Surrender(UserToken userToken, GameToken gameToken)
        {
            if (userToken == null)
                throw new ArgumentNullException(nameof(userToken));

            if (gameToken == null)
                throw new ArgumentNullException(nameof(gameToken));

            var dbGame = GameBaseSingleton.GetInstance();
            var dbUser = UserBaseSingleton.GetInstance();

            var user = dbUser.GetUser(userToken);
            var game = dbGame.Get(gameToken);

            if (user == null)
                throw new UserNotLoggedException(userToken.Username);

            game.Surrender(user);
        }
    }
}
