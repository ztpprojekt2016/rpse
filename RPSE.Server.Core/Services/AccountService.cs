﻿using System;
using System.Linq;
using RPSE.Server.Core.Implementation;
using RPSE.Server.Core.Implementation.Games;
using RPSE.Server.Core.Implementation.Games.State;
using RPSE.Server.Core.Implementation.Tokens;
using RPSE.Server.Core.Implementation.Users;
using RPSE.Shared.Auth;
using RPSE.Shared.Interfaces;

namespace RPSE.Server.Core.Services
{
    public class AccountService : IAccountService
    {
        public UserToken Login(string username, string password)
        {
            Console.WriteLine("Login called, username:" + username);

            var userBase = UserBaseSingleton.GetInstance();

            var user = userBase.GetUser(username);

            if (user == null)
                return null;

            if (user.Password != password)
                return null;

            user.Online = true;

            var tokenFactory = new TokenFactory();
            var token = tokenFactory.CreateUserToken(user.Username);
            user.UserToken = token;

            //userBase.Update(user); //todo

            return token;
        }

        public void Logout(UserToken userToken)
        {
            if (userToken == null)
                throw new ArgumentNullException(nameof(userToken));

            var userBase = UserBaseSingleton.GetInstance();

            var user = userBase.GetUser(userToken);

            user.Online = false;
            user.UserToken = null;

            var db = GameBaseSingleton.GetInstance();
            var userGames = db.GetAll()
                .Where(x => x.State == GameStateEnum.Started)
                .Where(x => x.Users.Any(y => y.User == user))
                .ToList();

            foreach (var game in userGames)
            {
                if (game.State == GameStateEnum.Started)
                    game.GameOver();
            }



            //userBase.Update(user); //todo
        }

        public bool Register(string username, string password)
        {
            var userBase = UserBaseSingleton.GetInstance();

            if (userBase.UserExist(username))
                return false;

            var user = new User //todo fabryka
            {
                Username = username,
                Password = password
            };

            userBase.AddUser(user);

            return true;
        }
    }
}
