﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RPSE.Shared.Enums;
using RPSE.Shared.Items;

namespace RPSE.Server.Core.Items
{
    [Serializable]
    public class Paper : Item
    {
        public Paper()
        {
            Name = "Papier";
            ImageUrl = "http://pool.spglek.linuxpl.info/rpse/paper.png";
        }

        protected override IList<string> BeatsId { get; } = new List<string>(new string[] { Rock.Sid, Spock.Sid });
        protected override string Id { get; } = Sid;
        public override GameTypeEnum Type { get; } = GameTypeEnum.Standard;
        public const string Sid = "Paper";
    }
}
