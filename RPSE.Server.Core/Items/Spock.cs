﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RPSE.Shared.Enums;
using RPSE.Shared.Items;

namespace RPSE.Server.Core.Items
{
    [Serializable]
    public class Spock : Item
    {
        public Spock()
        {
            Name = "Spock";
            ImageUrl = "http://pool.spglek.linuxpl.info/rpse/spock.png";
        }

        protected override IList<string> BeatsId { get; } = new List<string>(new string[] { Rock.Sid, Scissors.Sid });
        protected override string Id { get; } = Sid;
        public override GameTypeEnum Type { get; } = GameTypeEnum.Spock;
        public const string Sid = "Spock";
    }
}
