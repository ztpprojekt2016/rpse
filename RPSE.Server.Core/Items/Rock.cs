﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RPSE.Shared.Enums;
using RPSE.Shared.Items;

namespace RPSE.Server.Core.Items
{
    [Serializable]
    public class Rock : Item
    {
        public Rock()
        {
            Name = "Kamień";
            ImageUrl = "http://pool.spglek.linuxpl.info/rpse/rock.png";
        }

        protected override IList<string> BeatsId { get; } = new List<string>(new string[] { Scissors.Sid, Lizard.Sid });
        protected override string Id { get; } = Sid;
        public override GameTypeEnum Type { get; } = GameTypeEnum.Standard;
        public const string Sid = "Rock";
    }
}
