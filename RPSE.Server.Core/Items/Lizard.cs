﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RPSE.Shared.Enums;
using RPSE.Shared.Items;

namespace RPSE.Server.Core.Items
{
    [Serializable]
    public class Lizard : Item
    {
        public Lizard()
        {
            Name = "Jaszczur";
            ImageUrl = "http://pool.spglek.linuxpl.info/rpse/lizard.png";
        }

        protected override IList<string> BeatsId { get; } = new List<string>(new string[] { Paper.Sid, Spock.Sid });
        protected override string Id { get; } = Sid;
        public override GameTypeEnum Type { get; } = GameTypeEnum.Spock;
        public const string Sid = "Lizard";
    }
}
