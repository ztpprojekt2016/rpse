﻿using System;

namespace RPSE.Server.Core.Exceptions
{
    public class UserNotExistException : Exception
    {
        public UserNotExistException(string username) : base(username + " not exist")
        {
        }
    }
}
