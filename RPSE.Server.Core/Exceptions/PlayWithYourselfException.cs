﻿using System;

namespace RPSE.Server.Core.Exceptions
{
    class PlayWithYourselfException : Exception
    {
        public PlayWithYourselfException(string username) : base(username + " wanted play with himself!")
        {
        }
    }
}
