﻿using System;

namespace RPSE.Server.Core.Exceptions
{
    public class UserNotLoggedException : Exception
    {
        public UserNotLoggedException(string username) : base(username + " not logged")
        {
        }
    }
}
