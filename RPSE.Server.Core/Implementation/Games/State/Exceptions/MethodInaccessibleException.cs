﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPSE.Server.Core.Implementation.Games.State.Exceptions
{
    class MethodInaccessibleException : Exception
    {
        public MethodInaccessibleException() : base("method cannot be executed in this state")
        {
        }

        public MethodInaccessibleException(string method) : base(method + " method cannot be executed in this state")
        {
        }

        public MethodInaccessibleException(string method, string why) : base(method + " method cannot be executed in this state because " + why + "!")
        {
        }
    }
}
