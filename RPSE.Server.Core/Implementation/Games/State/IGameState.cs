﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RPSE.Server.Core.Game;
using RPSE.Server.Core.Implementation.Users;
using RPSE.Shared.Commands;
using RPSE.Shared.Conteners;
using RPSE.Shared.Enums;
using RPSE.Shared.Items;

namespace RPSE.Server.Core.Implementation.Games.State
{
    public interface IGameState
    {
        GameStateEnum State { get; }

        void AddUserToGame(User user);

        void Start();

        bool Init(User user, GameOverCommand gameOverCommand, RoundOverCommand roundOverCommand,
            RoundStartCommand roundStartCommand);

        void SelectItem(User user, Item item);
        void RoundStart();

        void RoundOver();
        void GameOver();

        void Surrender(User user);
    }
}
