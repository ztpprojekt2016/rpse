﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RPSE.Server.Core.Implementation.Games.State.Exceptions;
using RPSE.Server.Core.Implementation.Users;
using RPSE.Shared.Commands;
using RPSE.Shared.Items;

namespace RPSE.Server.Core.Implementation.Games.State
{

    class GameInitialized : IGameState
    {
        private Game game;

        public GameStateEnum State { get; } = GameStateEnum.Initialized;

        public GameInitialized(Game game)
        {
            this.game = game;
        }

        public void AddUserToGame(User user)
        {
            throw new GameInitializedException("AddUserToGame");
        }

        public void Start()
        {
            game.GameState = new GameStarted(game);
            game.GameState.RoundStart();
        }

        public bool Init(User user, GameOverCommand gameOverCommand, RoundOverCommand roundOverCommand,
            RoundStartCommand roundStartCommand)
        {
            throw new GameInitializedException("Init");
        }

        public void SelectItem(User user, Item item)
        {
            throw new GameInitializedException("SelectItem");
        }

        public void RoundStart()
        {
            throw new GameInitializedException("RoundStart");
        }

        public void RoundOver()
        {
            throw new GameInitializedException("RoundOver");
        }

        public void GameOver()
        {
            throw new GameInitializedException("GameOver");
        }

        public void Surrender(User user)
        {
            throw new GameInitializedException("Surrender");
        }

        public class GameInitializedException : MethodInaccessibleException
        {
            public GameInitializedException(string method) : base(method, "game is initialized and not started") { }
        }
    }
}
