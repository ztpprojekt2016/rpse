﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RPSE.Server.Core.Implementation.Games.State.Exceptions;
using RPSE.Server.Core.Implementation.Users;
using RPSE.Shared.Commands;
using RPSE.Shared.Items;

namespace RPSE.Server.Core.Implementation.Games.State
{
    class GameNotInitialized : IGameState
    {
        private Game game;

        public GameNotInitialized(Game game)
        {
            this.game = game;
        }

        public GameStateEnum State { get; } = GameStateEnum.NotInitialized;

        public void AddUserToGame(User user)
        {
            var gameInfo = new UserGameInfo();
            gameInfo.User = user;

            game.Users.Add(gameInfo);
        }

        public void GameOver()
        {
            throw new GameNotInitializedException("GameOver");
        }

        public bool Init(User user, GameOverCommand gameOverCommand, RoundOverCommand roundOverCommand, RoundStartCommand roundStartCommand)
        {
            var userGame = game.Users.SingleOrDefault(x => x.User == user);

            if (userGame == null)
                return false;

            userGame.OnGameOver = gameOverCommand;
            userGame.OnRoundOver = roundOverCommand;
            userGame.OnRoundStart = roundStartCommand;

            //
            if (game.Users.Any(x => x.OnGameOver == null ||
                                    x.OnRoundOver == null ||
                                    x.OnRoundStart == null))
                return false;

            game.GameState = new GameInitialized(game);
            return true;
        }

        public void RoundOver()
        {
            throw new GameNotInitializedException("RoundOver");
        }

        public void RoundStart()
        {
            throw new GameNotInitializedException("RoundStart");
        }

        public void SelectItem(User user, Item item)
        {
            throw new GameNotInitializedException("SelectItem");
        }

        public void Start()
        {
            throw new GameNotInitializedException("Start");
        }

        public void Surrender(User user)
        {
            throw new GameNotInitializedException("Surrender");
        }

        public class GameNotInitializedException : MethodInaccessibleException
        {
            public GameNotInitializedException(string method) : base(method, "game isnt initialized") { }
        }
    }
}
