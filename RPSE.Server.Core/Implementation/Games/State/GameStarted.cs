﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RPSE.Server.Core.Game;
using RPSE.Server.Core.Implementation.Users;
using RPSE.Shared.Commands;
using RPSE.Shared.Conteners;
using RPSE.Shared.Enums;
using RPSE.Shared.Items;
using RPSE.Server.Core.Implementation.Games.State.Exceptions;

namespace RPSE.Server.Core.Implementation.Games.State
{
    class GameStarted : IGameState
    {
        private Game game;
        //private Dictionary<User, List<Item>> _history = new Dictionary<User, List<Item>>();//todo

        public GameStarted(Game game)
        {
            this.game = game;
        }

        private int Round { get; set; }

        public GameStateEnum State { get; } = GameStateEnum.Started;

        public void AddUserToGame(User user)
        {
            throw new GameStartedException("AddUserToGame");
        }

        public void GameOver()
        {
            //todo przepisanie punktów do konta usera

            double average = game.Users.Select(x => x.Points).Average();

            foreach (var user in game.Users)
            {
                var info = new GameOverInfo();
                info.Points = user.Points;

                if (user.Points > average)
                {
                    info.Result = GameResultEnum.Won;
                }
                else if (user.Points < average)
                {
                    info.Result = GameResultEnum.Lost;
                }
                else
                {
                    info.Result = GameResultEnum.Draw;
                }
                Task.Run(() =>
                {
                    user.OnGameOver.Execute(info);
                });
            }

            //xxx dodać worker żeby  usunął gre
            game.GameState = new GameEnded();
        }

        public bool Init(User user, GameOverCommand gameOverCommand, RoundOverCommand roundOverCommand, RoundStartCommand roundStartCommand)
        {
            throw new GameStartedException("Init");
        }

        public void RoundOver()
        {
            Dictionary<string, Item> choices = new Dictionary<string, Item>();

            foreach (var user in game.Users)
            {//sprawdzamy z iloma userami wygrał dany user, i tak dla każdego
                foreach (var opponent in game.Users.Where(x => x != user))
                {
                    if (user.SelectedItem.TryWin(opponent.SelectedItem))
                    {
                        user.RoundPoints++;
                    }
                }

                choices.Add(user.User.Username, user.SelectedItem);
            }

            double average = game.Users.Select(x => x.RoundPoints).Average();//liczymy średnią punktów zdobytych w walce

            foreach (var user in game.Users)
            {
                var info = new RoundOverInfo();
                info.Points = user.RoundPoints;
                info.Choices = choices;
                if (user.RoundPoints > average)
                {
                    info.Result = GameResultEnum.Won;
                }
                else if (user.RoundPoints < average)
                {
                    info.Result = GameResultEnum.Lost;
                }
                else
                {
                    info.Result = GameResultEnum.Draw;
                }

                user.OnRoundOver.Execute(info);
            }


            foreach (var user in game.Users)
            {
                user.Points += user.RoundPoints;
                user.RoundPoints = 0;
                user.History.Add(user.SelectedItem);
                user.SelectedItem = null;
            }
        }

        public void RoundStart()
        {
            var itemFactory = new ItemFactory();

            Round++;

            GameTypeEnum type;
            switch (new Random().Next(10))
            {
                case 1:
                    type = GameTypeEnum.Spock;
                    break;
                default:
                    type = GameTypeEnum.Standard;
                    break;
            }

            foreach (var user in game.Users)
            {
                var info = new RoundInfo();
                info.Player = user.User.Username;
                info.Users = game.Users
                    .Select(x => x.User.Username)
                    .Where(x => x != info.Player)
                    .ToList();
                info.Items = itemFactory.GetItems(type);
                info.Round = Round;
                user.OnRoundStart.Execute(info);
            }
        }

        private object selectItemLock = new object();

        public void SelectItem(User user, Item item)
        {
            var userGame = game.Users.SingleOrDefault(x => x.User == user);

            if (userGame == null)
                return;

            userGame.SelectedItem = item;

            if (game.Users.All(x => x.SelectedItem != null))
            {
                lock (selectItemLock)
                {
                    if (game.Users.All(x => x.SelectedItem != null))
                    {
                        Task.Run(() =>
                        {
                            RoundOver();

                            Task.Delay(3000).Wait();

                            if (Round < 4)
                                RoundStart();
                            else
                                GameOver();
                        });
                    }
                }
            }
        }

        public void Start()
        {
            //throw new GameStartedException("Start");
        }

        public void Surrender(User user)
        {
            // todo
        }

        public class GameStartedException : MethodInaccessibleException
        {
            public GameStartedException(string method) : base(method, "game is started") { }
        }
    }
}
