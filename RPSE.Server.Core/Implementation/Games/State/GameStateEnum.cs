﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPSE.Server.Core.Implementation.Games.State
{
    public enum GameStateEnum
    {
        NotInitialized,
        Initialized,
        Started,
        Ended
    }
}
