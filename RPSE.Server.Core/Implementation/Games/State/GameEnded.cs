﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RPSE.Server.Core.Implementation.Games.State.Exceptions;
using RPSE.Server.Core.Implementation.Users;
using RPSE.Shared.Commands;
using RPSE.Shared.Items;

namespace RPSE.Server.Core.Implementation.Games.State
{
    class GameEnded : IGameState
    {
        public GameStateEnum State { get; } = GameStateEnum.Ended;

        public void AddUserToGame(User user)
        {
            throw new GameEndedException("AddUserToGame");
        }

        public void Start()
        {
            throw new GameEndedException("Start");
        }

        public bool Init(User user, GameOverCommand gameOverCommand, RoundOverCommand roundOverCommand,
            RoundStartCommand roundStartCommand)
        {
            throw new GameEndedException("Init");
        }

        public void SelectItem(User user, Item item)
        {
            throw new GameEndedException("SelectItem");
        }

        public void RoundStart()
        {
            throw new GameEndedException("RoundStart");
        }

        public void RoundOver()
        {
            throw new GameEndedException("RoundOver");
        }

        public void GameOver()
        {
            throw new GameEndedException("GameOver");
        }

        public void Surrender(User user)
        {
            throw new GameEndedException("Surrender");
        }

        public class GameEndedException : MethodInaccessibleException
        {
            public GameEndedException(string method) : base(method, "game is ended") { }
        }
    }
}
