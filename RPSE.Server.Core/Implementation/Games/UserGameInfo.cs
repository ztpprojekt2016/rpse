﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RPSE.Server.Core.Implementation.Users;
using RPSE.Shared.Commands;
using RPSE.Shared.Items;

namespace RPSE.Server.Core.Implementation.Games
{
    public class UserGameInfo
    {
        public User User { get; set; }
        public RoundStartCommand OnRoundStart { get; set; }
        public RoundOverCommand OnRoundOver { get; set; }
        public GameOverCommand OnGameOver { get; set; }
        public int Points { get; set; }

        public IList<Item> History { get; set; } = new List<Item>();
        public Item SelectedItem { get; set; }
        public int RoundPoints { get; set; }
    }
}
