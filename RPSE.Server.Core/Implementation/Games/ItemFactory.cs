﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Design;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RPSE.Server.Core.Extensions;
using RPSE.Server.Core.Items;
using RPSE.Shared.Enums;
using RPSE.Shared.Items;

namespace RPSE.Server.Core.Game
{//WZORZEC PROTOTYP
    public class ItemFactory
    {
        private Item[] _items = new Item[] { new Rock(), new Paper(), new Scissors(), new Spock(), new Lizard() };

        public Item[] GetItems(GameTypeEnum gameType)
        {
            if (gameType == GameTypeEnum.Standard)
            {
                var items = _items.Where(x => x.Type == GameTypeEnum.Standard)
                    .ToArray();

                return (Item[])items.Clone();
            }
            else if (gameType == GameTypeEnum.Spock)
            {
                var items = _items.Where(x => x.Type == GameTypeEnum.Standard || x.Type == GameTypeEnum.Spock)
                    .ToArray();

                return (Item[])items.Clone();
            }

            return null;
        }
    }
}
