﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RPSE.Server.Core.Implementation.Users;
using RPSE.Shared.Auth;

namespace RPSE.Server.Core.Implementation.Games
{
    class GameBaseSingleton
    {
        private static GameBaseSingleton _instance;
        private List<Game> games = new List<Game>();
        private static object _object = new object();

        private GameBaseSingleton()
        {
        }

        public static GameBaseSingleton GetInstance()
        {
            if (_instance == null)
            {
                lock (_object)
                {
                    _instance = new GameBaseSingleton();
                }
            }

            return _instance;
        }

        public void Add(Game game)
        {
            games.Add(game);
        }

        public Game Get(GameToken gameToken)
        {
            return games.SingleOrDefault(x => x.GameToken.Equals(gameToken));
        }

        public IList<Game> GetAll()
        {
            return games;
        }

        public void Remove(GameToken gameToken)
        {
            var game = Get(gameToken);

            if (game != null)
                Remove(game);
        }

        public void Remove(Game game)
        {
            games.Remove(game);
        }
    }
}
