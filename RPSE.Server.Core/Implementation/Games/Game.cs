﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RPSE.Server.Core.Game;
using RPSE.Server.Core.Implementation.Games.State;
using RPSE.Server.Core.Implementation.Users;
using RPSE.Server.Core.Items;
using RPSE.Shared.Auth;
using RPSE.Shared.Commands;
using RPSE.Shared.Conteners;
using RPSE.Shared.Enums;
using RPSE.Shared.Items;

namespace RPSE.Server.Core.Implementation.Games
{
    public class Game
    {
        public IGameState GameState;

        public GameStateEnum State
        {
            get { return GameState.State; }
        }

        public GameToken GameToken { get; set; }
        public IList<UserGameInfo> Users = new List<UserGameInfo>();


        public Game()
        {
            GameState = new GameNotInitialized(this);
        }

        public void AddUserToGame(User user)
        {
            GameState.AddUserToGame(user);
        }

        public void Start()
        {
            GameState.Start();
        }

        public bool Init(User user, GameOverCommand gameOverCommand, RoundOverCommand roundOverCommand, RoundStartCommand roundStartCommand)
        {
            return GameState.Init(user, gameOverCommand, roundOverCommand, roundStartCommand);
        }

        public void SelectItem(User user, Item item)
        {
            GameState.SelectItem(user, item);
        }

        public void RoundStart()
        {
            GameState.RoundStart();
        }

        public void RoundOver()
        {
            GameState.RoundOver();
        }

        public void GameOver()
        {
            GameState.GameOver();
        }

        public void Surrender(User user)
        {
            GameState.Surrender(user);
        }

    }
}
