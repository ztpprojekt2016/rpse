﻿using System;
using RPSE.Shared.Auth;

namespace RPSE.Server.Core.Implementation.Tokens
{//WZORZEC metoda fabrykująca
    public class TokenFactory
    {
        public GameToken CreateGameToken()
        {
            return new GameToken(Guid.NewGuid().ToString());
        }

        public UserToken CreateUserToken(string username)
        {
            return new UserToken(username, Guid.NewGuid().ToString());
        }

        public OfferToken CreateOfferToken()
        {
            return new OfferToken(Guid.NewGuid().ToString());
        }
    }
}
