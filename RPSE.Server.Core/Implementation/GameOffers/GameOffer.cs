﻿using RPSE.Server.Core.Implementation.Users;
using RPSE.Shared.Auth;
using RPSE.Shared.Commands;

namespace RPSE.Server.Core.Implementation.GameOffers
{
    public class GameOffer
    {
        public OfferToken OfferToken { get; set; }

        public User User1 { get; set; }
        public User User2 { get; set; }

        public bool Accepted { get; set; }

        public GameEstablishedCommand OnAccepted { get; set; }
        public GameOfferRejectedCommand OnRejected { get; set; }
    }

}
