﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RPSE.Server.Core.Implementation.GameOffers;
using RPSE.Shared.Auth;

namespace RPSE.Server.Core.Implementation.GameOffers
{
    public class GameOfferSingleton
    {
        private static GameOfferSingleton _instance;
        private List<GameOffer> offers = new List<GameOffer>();
        private static object _object = new object();

        private GameOfferSingleton()
        {
        }

        public static GameOfferSingleton GetInstance()
        {
            if (_instance == null)
            {
                lock (_object)
                {
                    _instance = new GameOfferSingleton();
                }
            }

            return _instance;
        }


        public void Add(GameOffer offer)
        {
            offers.Add(offer);
        }


        public GameOffer GetByOfferToken(OfferToken offerToken)
        {
            return offers.SingleOrDefault(x => x.OfferToken.Equals(offerToken));
        }

        internal void Remove(GameOffer offer)
        {
            offers.Remove(offer);
        }
    }
}
