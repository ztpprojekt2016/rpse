﻿using System;
using System.Collections.Generic;
using System.Linq;
using RPSE.Shared.Auth;

namespace RPSE.Server.Core.Implementation.Users
{//WZORZEC singleton
    public class UserBaseSingleton
    {
        private static UserBaseSingleton _instance;
        private List<User> users = new List<User>();
        private static object _object = new object();

        private UserBaseSingleton()
        {
        }

        public static UserBaseSingleton GetInstance()
        {
            if (_instance == null)
            {
                lock (_object)
                {
                    _instance = new UserBaseSingleton();
                }
            }

            return _instance;
        }

        public void AddUser(User user)
        {
            if (UserExist(user))
                return;

            users.Add(user);
        }

        public bool UserExist(User user)
        {
            return users.Contains(user);
        }

        public bool UserExist(string username)
        {
            return users.Any(x => x.Username == username);
        }

        public User GetUser(UserToken userToken)
        {
            return users
                .Where(x=>x.UserToken != null)
                .SingleOrDefault(x => x.UserToken.Equals(userToken));
        }

        public User GetUser(string username)
        {
            return users.SingleOrDefault(x => x.Username == username);
        }

        public IList<User> GetOnlineUsers()
        {
            return users.Where(x => x.Online).ToList();
        }
    }
}
