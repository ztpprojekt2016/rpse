﻿using System.Collections;
using System.Collections.Generic;
using RPSE.Shared.Auth;
using RPSE.Shared.Commands;

namespace RPSE.Server.Core.Implementation.Users
{
    public class User
    {
        public User()
        {
            Games = new List<GameToken>();
        }

        public string Username { get; set; }
        public string Password { get; set; }
        public int Level { get; set; }
        public bool Online { get; set; }
        public bool Ready { get; set; }
        public UserToken UserToken { get; set; }//aktywna sesja może być tylko jedna
        public IList<GameToken> Games { get; set; }//fixme wywalic
        public GameOfferCommand GameOfferCommand { get; internal set; }
    }
}
