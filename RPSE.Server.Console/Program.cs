﻿using System;
using System.CodeDom;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RPSE.Server.Core.Services;
using RPSE.Server.BotConsole.Bots;

namespace RPSE.Server.BotConsole
{
    public class Program //odpalamy serwer tutaj :)
    {
        public static void Main(string[] args)
        {
            var p = new Program();
            p.Run();
        }

        public void Run()
        {// tworzymy bota przykłądowego, tutaj też otwieramy nasłuchiwanie itp
            var bot = new Bot();
            bot.AccountService = new AccountService();
            bot.GameService = new GameService();
            bot.Init();
            bot.ReadyToGame();


        }
    }
}
