﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RPSE.Shared.Commands;
using RPSE.Shared.Conteners;

namespace RPSE.Server.BotConsole.Bots.Commands
{
    public class GameOverBotCommand : GameOverCommand
    {
        private BotGame botGame;

        public GameOverBotCommand()
        {
        }

        public GameOverBotCommand(BotGame botGame)
        {
            this.botGame = botGame;
        }

        public override void Execute(GameOverInfo gameInfo)
        {
            //TODO
        }
    }
}
