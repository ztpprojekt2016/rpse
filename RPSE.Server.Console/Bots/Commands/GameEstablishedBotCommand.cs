﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RPSE.Shared.Auth;
using RPSE.Shared.Commands;

namespace RPSE.Server.BotConsole.Bots.Commands
{
    public class GameEstablishedBotCommand : GameEstablishedCommand
    {
        private Bot _bot;

        public GameEstablishedBotCommand(Bot bot)
        {
            this._bot = bot;
        }

        public override void Execute(GameToken gameToken)
        {
            _bot.GameEstablished(gameToken);
        }
    }
}
