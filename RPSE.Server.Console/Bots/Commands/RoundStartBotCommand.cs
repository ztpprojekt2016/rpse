﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RPSE.Shared.Commands;
using RPSE.Shared.Conteners;

namespace RPSE.Server.BotConsole.Bots.Commands
{
    public class RoundStartBotCommand : RoundStartCommand
    {
        private BotGame botGame;

        public RoundStartBotCommand()
        {
        }

        public RoundStartBotCommand(BotGame botGame)
        {
            this.botGame = botGame;
        }

        public override void Execute(RoundInfo roundInfo)
        {
            botGame.RoundStart(roundInfo);
        }
    }
}
