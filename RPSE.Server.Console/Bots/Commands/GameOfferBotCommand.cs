﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RPSE.Shared.Auth;
using RPSE.Shared.Commands;
using RPSE.Shared.Conteners;

namespace RPSE.Server.BotConsole.Bots.Commands
{
    public class GameOfferBotCommand : GameOfferCommand
    {
        private Bot _bot;

        public GameOfferBotCommand(Bot bot)
        {
            _bot = bot;
        }

        public override void Execute(OfferToken offerToken, OfferInfo offerInfo)
        {
            _bot.GameOffer(offerToken, offerInfo);
        }
    }
}
