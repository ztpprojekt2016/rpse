﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPSE.Server.BotConsole.Bots
{
    public class BotBadPasswordException : Exception
    {
        public BotBadPasswordException(string login, string password)
            :base("Password is incorrect! login:" + login + " password:" + password)
        {
        }
    }
}
