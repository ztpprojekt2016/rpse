﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPSE.Server.BotConsole.Bots
{
    public class BotLoginUnexpectedException : Exception
    {
        public BotLoginUnexpectedException(string login, string password)
            :base("Unexcepted login error! login:" + login + " password:" + password)
        {
        }
    }
}
