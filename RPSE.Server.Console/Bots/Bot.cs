﻿using System;
using RPSE.Shared.Auth;
using RPSE.Shared.Commands;
using RPSE.Shared.Conteners;
using RPSE.Shared.Interfaces;
using RPSE.Server.BotConsole.Bots.Commands;

namespace RPSE.Server.BotConsole.Bots
{
    public class Bot
    {//to nie jest zrozumiały kod..
        private Random rand = new Random();

        public IGameService GameService { get; set; }
        public IAccountService AccountService { get; set; }

        public string Login { get; set; } = "Bot";
        public string Password { get; set; } = "Bot123123";
        private UserToken UserToken { get; set; }

        public void Init() // jeden gracz może mieć wiele walk więc tworzymy tylko jednego bota
        {//logujemy po prostu bota do systemu
            if (UserToken != null)
                return;

            var token = AccountService.Login(Login, Password);
            if (token == null)//nie da rady zalogować to próbujemy zarejestrować ;)
            {
                if (!AccountService.Register(Login, Password))
                    throw new BotBadPasswordException(Login, Password);//konto istnieje a nie możemy się zalogować

                token = AccountService.Login(Login, Password);
            }

            if (token == null) //jakiś inny problem ;/
                throw new BotLoginUnexpectedException(Login, Password);

            UserToken = token;
        }

        internal void RoundStart(BotGame botGame, RoundInfo roundInfo)
        {
            var items = roundInfo.Items;

            var item = items[rand.Next(items.Length - 1)];

            GameService.SelectItem(UserToken, botGame.GameToken, item);
        }

        public void GameOffer(OfferToken offerToken, OfferInfo offerInfo)
        {
            GameService.AcceptOffer(UserToken, offerToken, new GameEstablishedBotCommand(this));
        }

        public void GameEstablished(GameToken gameToken)
        {
            var botGame = new BotGame(this);
            botGame.GameToken = gameToken;
            botGame.GameOverCommand = new GameOverBotCommand(botGame);
            botGame.RoundStartCommand = new RoundStartBotCommand(botGame);
            botGame.RoundOverCommand = new RoundOverBotCommand(botGame);
            GameService.InitGame(UserToken, botGame.GameToken, botGame.RoundStartCommand, 
                botGame.RoundOverCommand, botGame.GameOverCommand);
            //todo add to list
        }

        public void ReadyToGame()
        {
            GameService.WaitToGame(UserToken, new GameOfferBotCommand(this));
        }

        public void NotReadyToGame()
        {
            GameService.DontWaitToGame(UserToken);
        }

        public void Close()
        {
            AccountService.Logout(UserToken);
            UserToken = null;
        }
    }
}
