﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RPSE.Shared.Auth;
using RPSE.Shared.Commands;
using RPSE.Shared.Conteners;

namespace RPSE.Server.BotConsole.Bots
{
    public class BotGame
    {
        private Bot bot;

        public BotGame(Bot bot)
        {
            this.bot = bot;
        }

        public GameToken GameToken { get; set; }
        public RoundStartCommand RoundStartCommand { get; set; }
        public RoundOverCommand RoundOverCommand { get; set; }
        public GameOverCommand GameOverCommand { get; set; }

        internal void RoundStart(RoundInfo roundInfo)
        {
            bot.RoundStart(this, roundInfo);
        }
    }
}
