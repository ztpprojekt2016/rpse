﻿using RPSE.Client.UI.WPF.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace RPSE.Client.UI.WPF.Views
{
    /// <summary>
    /// Interaction logic for RejectWindow.xaml
    /// </summary>
    public partial class RejectWindow : Window
    {
        public RejectWindow(string username)
        {
            InitializeComponent();
            this.DataContext = new RejectViewModel(username);
        }
    }
}
