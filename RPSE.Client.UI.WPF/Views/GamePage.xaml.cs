﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using RPSE.Shared.Auth;
using RPSE.Client.UI.WPF.ViewModels;

namespace RPSE.Client.UI.WPF.Views
{
    /// <summary>
    /// Interaction logic for GamePage.xaml
    /// </summary>
    public partial class GamePage : Page
    {
        public GamePage(Context context, GameToken gameToken)
        {
            InitializeComponent();
            this.DataContext = new GameViewModel(context, gameToken, this);
        }
    }
}
