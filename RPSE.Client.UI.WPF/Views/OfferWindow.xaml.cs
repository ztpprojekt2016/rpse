﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using RPSE.Shared.Auth;
using RPSE.Shared.Conteners;
using RPSE.Client.UI.WPF.ViewModels;

namespace RPSE.Client.UI.WPF.Views
{
    /// <summary>
    /// Interaction logic for OfferWindow.xaml
    /// </summary>
    public partial class OfferWindow : Window
    {

        public OfferWindow(OfferToken offerToken, OfferInfo offerInfo, Context context)
        {
            InitializeComponent();
            this.DataContext = new OfferViewModel(offerToken, offerInfo, context);
        }
    }
}
