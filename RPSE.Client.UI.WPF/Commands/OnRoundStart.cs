﻿using RPSE.Shared.Commands;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RPSE.Shared.Conteners;
using RPSE.Client.UI.WPF.Views;
using System.Windows.Controls;
using System.Windows.Data;
using RPSE.Client.UI.WPF.ViewModels;
using System.Windows;

namespace RPSE.Client.UI.WPF.Commands
{
    class OnRoundStart : RoundStartCommand
    {
        private GameViewModel gameViewModel;

        public OnRoundStart(GameViewModel gameViewModel)
        {
            this.gameViewModel = gameViewModel;
        }

        public override void Execute(RoundInfo roundInfo)
        {
            gameViewModel.gamePage.Dispatcher.Invoke(() =>
            {
                gameViewModel.gamePage.info.Text = "";
                gameViewModel.gamePage.enemy.Text = roundInfo.Users[0];
                gameViewModel.gamePage.player.Text = roundInfo.Player;

                gameViewModel.gamePage.enemyItems.Children.Clear();
                foreach (var i in roundInfo.Items)
                {
                    Button button = new Button();
                    button.Content = i.ImageUrl;
                    button.Width = 120;
                    button.Height = 120;
                    button.Tag = i.Name;
                    Thickness margin = button.Margin;
                    margin.Left = 20;
                    margin.Right = 20;
                    margin.Top = 20;
                    margin.Bottom = 20;
                    button.Margin = margin;
                    Style style = gameViewModel.gamePage.FindResource("enemyItem") as Style;
                    button.Style = style;
                    button.IsEnabled = false;
                    gameViewModel.gamePage.enemyItems.Children.Add(button);
                }

                gameViewModel.items = roundInfo.Items;
                gameViewModel.gamePage.playerItems.Children.Clear();
                foreach (var i in roundInfo.Items)
                {
                    Button button = new Button();
                    button.Content = i.ImageUrl;
                    button.Tag = i.Name;
                    button.Width = 120;
                    button.Height = 120;
                    Thickness margin = button.Margin;
                    margin.Left = 20;
                    margin.Right = 20;
                    margin.Top = 20;
                    margin.Bottom = 20;
                    button.Margin = margin;
                    Style style = gameViewModel.gamePage.FindResource("Item") as Style;
                    button.Style = style;
                    Binding bind = new Binding();
                    bind.Path = new System.Windows.PropertyPath("SelectItemCmd");
                    button.SetBinding(Button.CommandProperty, bind);
                    Binding self = new Binding();
                    self.RelativeSource = new RelativeSource(RelativeSourceMode.Self);
                    button.SetBinding(Button.CommandParameterProperty, self);
                    gameViewModel.gamePage.playerItems.Children.Add(button);
                }
            });  
        }
    }
}
