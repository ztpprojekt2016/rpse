﻿using RPSE.Client.UI.WPF.Views;
using RPSE.Shared.Auth;
using RPSE.Shared.Commands;
using RPSE.Shared.Conteners;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPSE.Client.UI.WPF.Commands
{
    class OnGameOffer : GameOfferCommand
    {
        private Context context;

        public OnGameOffer(Context context)
        {
            this.context = context;
        }

        public override void Execute(OfferToken offerToken, OfferInfo offerInfo)
        {
            context.Frame.Dispatcher.Invoke(() =>
            {
                OfferWindow window = new OfferWindow(offerToken, offerInfo, context);
                window.Show();
            });
        }
    }
}
