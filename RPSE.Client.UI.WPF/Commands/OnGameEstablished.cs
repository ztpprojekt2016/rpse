﻿using RPSE.Shared.Commands;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RPSE.Shared.Auth;
using RPSE.Client.UI.WPF.Views;

namespace RPSE.Client.UI.WPF.Commands
{
    class OnGameEstablished : GameEstablishedCommand
    {
        private Context context;

        public OnGameEstablished(Context context)
        {
            this.context = context;
        }

        public override void Execute(GameToken gameToken)
        {
            context.Frame.Dispatcher.Invoke(() => {
                context.SetPage(new GamePage(context, gameToken));
            });
        }
    }
}
