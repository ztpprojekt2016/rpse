﻿using RPSE.Shared.Commands;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RPSE.Shared.Conteners;
using RPSE.Client.UI.WPF.Views;
using RPSE.Client.UI.WPF.ViewModels;
using RPSE.Client.UI.WPF.Extensions;
using System.Windows.Controls;
using RPSE.Shared.Items;

namespace RPSE.Client.UI.WPF.Commands
{
    class OnRoundOver : RoundOverCommand
    {
        private GameViewModel gameViewModel;

        public OnRoundOver(GameViewModel gameViewModel)
        {
            this.gameViewModel = gameViewModel;
        }

        public override void Execute(RoundOverInfo roundInfo)
        {
            gameViewModel.gamePage.Dispatcher.Invoke(() =>
            {
                foreach (KeyValuePair<string, Item> pair in roundInfo.Choices)
                {
                    if (pair.Key != gameViewModel.context.UserToken.Username)
                    {
                        foreach (object child in gameViewModel.gamePage.enemyItems.Children)
                        {
                            Button btn = child as Button;
                            if (btn.Tag.ToString() == pair.Value.Name)
                            {
                                btn.Opacity = 1;
                            }
                        }
                    }
                }
                String info = roundInfo.Result.ToNiceLabel();
                if (roundInfo.Result == Shared.Enums.GameResultEnum.Won)
                {
                    info += String.Format(" Zdobyłeś {0} punkt", roundInfo.Points);
                }
                gameViewModel.gamePage.info.Text = info;
            });
        }
    }
}
