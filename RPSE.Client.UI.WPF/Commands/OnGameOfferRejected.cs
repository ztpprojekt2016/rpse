﻿using RPSE.Client.UI.WPF.Views;
using RPSE.Shared.Commands;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPSE.Client.UI.WPF.Commands
{
    class OnGameOfferRejected : GameOfferRejectedCommand
    {
        private Context context;

        public OnGameOfferRejected(Context context)
        {
            this.context = context;
        }

        public override void Execute(string username)
        {
            context.Frame.Dispatcher.Invoke(() =>
            {
                RejectWindow window = new RejectWindow(username);
                window.Show();
            });
        }
    }
}
