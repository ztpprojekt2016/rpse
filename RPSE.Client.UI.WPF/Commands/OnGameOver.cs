﻿using RPSE.Shared.Commands;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RPSE.Shared.Conteners;
using RPSE.Client.UI.WPF.Views;
using RPSE.Client.UI.WPF.ViewModels;
using System.Windows.Controls;
using System.Windows;
using System.Windows.Data;
using RPSE.Client.UI.WPF.Extensions;

namespace RPSE.Client.UI.WPF.Commands
{
    class OnGameOver : GameOverCommand
    {
        private GameViewModel gameViewModel;

        public OnGameOver(GameViewModel gameViewModel)
        {
            this.gameViewModel = gameViewModel;
        }

        public override void Execute(GameOverInfo gameInfo)
        {
                gameViewModel.gamePage.Dispatcher.Invoke(() => {
                    gameViewModel.gamePage.enemyItems.Children.Clear();
                    gameViewModel.gamePage.playerItems.Children.Clear();
                    gameViewModel.gamePage.enemy.Text = "";
                    gameViewModel.gamePage.player.Text = "";
                    String info = gameInfo.Result.ToNiceLabel();
                    if (gameInfo.Result == Shared.Enums.GameResultEnum.Won)
                    {
                        info += String.Format(" W sumie zdobyłeś {0}p.", gameInfo.Points);
                    }
                    gameViewModel.gamePage.info.Text = info;
                    gameViewModel.gamePage.goToMenu.Visibility = Visibility.Visible;
            });
        }
    }
}
