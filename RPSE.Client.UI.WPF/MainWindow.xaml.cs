﻿using RPSE.Shared.Interfaces;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using RPSE.Client.Libs;
using RPSE.Client.Libs.Adapter;
using RPSE.Client.Libs.Proxy;
using RPSE.Shared.Auth;
using RPSE.Client.UI.WPF.Views;

namespace RPSE.Client.UI.WPF
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        [DllImport("Kernel32")]
        public static extern void AllocConsole();

        [DllImport("Kernel32")]
        public static extern void FreeConsole();

        private Context context;

        public MainWindow()
        {
            InitializeComponent();
            //AllocConsole();

            

            Console.WriteLine("Hi!");
            context = new Context(Main);
            context.SetPage(new LoginPage(context));
        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            if (context.UserToken != null)
            {
                context.AccountService.Logout(context.UserToken);
            }
        }
    }
}
