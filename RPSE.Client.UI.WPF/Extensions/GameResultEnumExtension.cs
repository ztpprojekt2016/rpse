﻿using RPSE.Shared.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPSE.Client.UI.WPF.Extensions
{
    public static class GameResultEnumExtension
    {
        public static string ToNiceLabel(this GameResultEnum val)
        {
            switch (val)
            {
                case Shared.Enums.GameResultEnum.Won:
                    return "Wygrałeś!";
                case Shared.Enums.GameResultEnum.Lost:
                    return "Przegrałeś!";
                case Shared.Enums.GameResultEnum.Draw:
                    return "Remis!";
                default:
                    return "Brak";
            }
        }
    }
}
