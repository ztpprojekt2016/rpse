﻿using RPSE.Client.Libs.Adapter;
using RPSE.Client.Libs.Proxy;
using RPSE.Shared.Auth;
using RPSE.Shared.Interfaces;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using RPSE.Client.Libs;

namespace RPSE.Client.UI.WPF
{
    public class Context
    {
        public UserToken UserToken { get; set; }
        public Frame Frame { get; set; }
        public IAccountService AccountService { get; set; }
        public GameAdapter GameAdapter { get; set; }
        public Context(Frame frame)
        {
            var gameService = new GameServiceProxy();
            var accountService = new AccountServiceProxy();

            this.AccountService = accountService;
            this.GameAdapter = new GameAdapter(gameService);
            this.Frame = frame;
            this.UserToken = null;

            string ip = "";
            try
            {
                using (StreamReader reader = new StreamReader("ip.txt"))
                {
                    ip = reader.ReadLine();
                }
            }
            catch (Exception)
            {
                MessageBox.Show("nie znajduję pliku ip.txt");
                Environment.Exit(0);
            }

            TcpClient tcpClient = new TcpClient(ip);
            gameService.TcpClient = tcpClient;
            tcpClient.GameServiceProxy = gameService;
            accountService.TcpClient = tcpClient;
            tcpClient.AccountServiceProxy = accountService;

            try
            {
                tcpClient.Init();
            }
            catch (Exception ex)
            {
                MessageBox.Show("serwer jest niedostępny");
                Environment.Exit(0);
                //Application.Current.Shutdown();
            }
        }

        public void SetPage(Page page)
        {
            this.Frame.Content = page;
        }
    }
}
