﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RPSE.Shared.Auth;
using RPSE.Client.Libs.Proxy;
using RPSE.Client.UI.WPF.Commands;
using RPSE.Client.Libs.Adapter;
using RPSE.Client.UI.WPF.Views;
using System.Windows.Input;
using System.Windows.Controls;
using RPSE.Shared.Items;

namespace RPSE.Client.UI.WPF.ViewModels
{
    class GameViewModel
    {
        public Context context;
        public GamePage gamePage;
        public GameToken gameToken;
        public Item[] items;

        public ICommand SurrenderCmd { get; set; }
        public ICommand SelectItemCmd { get; set; }
        public ICommand GoToMainCmd { get; set; }

        public GameViewModel(Context context, GameToken gameToken, GamePage gamePage)
        {
            this.context = context;
            this.gameToken = gameToken;
            this.gamePage = gamePage;
            SurrenderCmd = new RelayCommand(pars => Surrender(pars));
            SelectItemCmd = new RelayCommand(pars => SelectItem(pars));
            GoToMainCmd = new RelayCommand(pars => gotToMain());
            this.context.GameAdapter.InitGame(this.gameToken, new OnRoundStart(this), new OnRoundOver(this), new OnGameOver(this));
        }

        public void SelectItem(object parameter)
        { 
            Button button = parameter as Button;
            string name = button.Tag.ToString();
            Item item = items.FirstOrDefault(x => x.Name == name);

            context.GameAdapter.SelectItem(gameToken, item);
        }
        public void Surrender(object paramteter)
        {
            context.GameAdapter.Surrender(gameToken);
        }

        public void gotToMain()
        {
            context.SetPage(new MainPage(context));
        }
    }
}
