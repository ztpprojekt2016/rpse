﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RPSE.Shared.Auth;
using RPSE.Shared.Conteners;
using System.Windows.Input;
using RPSE.Client.Libs.Proxy;
using System.Windows;
using RPSE.Client.UI.WPF.Commands;
using RPSE.Client.Libs.Adapter;

namespace RPSE.Client.UI.WPF.ViewModels
{
    class OfferViewModel
    {
        private OfferInfo offerInfo;
        private OfferToken offerToken;
        private Context context;

        public String OfferedBy
        {
            get
            {
                return String.Format("Zaproszenie do gry od {0}", offerInfo.Username);
            }
        }

        public ICommand AcceptOfferCmd { get; set; }
        public ICommand RejectOfferCmd { get; set; }

        public OfferViewModel(OfferToken offerToken, OfferInfo offerInfo, Context context)
        {
            this.offerToken = offerToken;
            this.offerInfo = offerInfo;
            this.context = context;
            AcceptOfferCmd = new RelayCommand(pars => AcceptOffer(pars));
            RejectOfferCmd = new RelayCommand(pars => RejectOffer(pars));
        }

        public void AcceptOffer(object parameter)
        {
            context.GameAdapter.AcceptOffer(offerToken, new OnGameEstablished(context));
            Window window = parameter as Window;
            if (window != null)
            {
                window.Close();
            }
        }

        public void RejectOffer(object parameter)
        {
            context.GameAdapter.RejectOffer(offerToken);
            Window window = parameter as Window;
            if(window != null)
            {
                window.Close();
            }
        }
    }
}
