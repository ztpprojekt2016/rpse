﻿using RPSE.Client.UI.WPF.Commands;
using RPSE.Client.UI.WPF.Conventer;
using RPSE.Client.UI.WPF.Views;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Shapes;

namespace RPSE.Client.UI.WPF.ViewModels
{
    public class MainViewModel
    {
        private Context Context;
        public string GetUsername
        {
            get
            {
                return String.Format("Zalogowany jako {0}", Context.UserToken.Username);
            }
        }
        public ObservableCollection<String> Players { get; set; }
        private List<string> users = new List<string>();
        public bool IsRed { get; set; }
        public ICommand LogoutCmd { get; set; }
        public ICommand ChangeStatusCmd { get; set; }
        public ICommand InviteToPlayCmd { get; set; }
        public MainViewModel(Context context)
        {
            this.Context = context;
            this.Players = ObservableConverter.ToObservableCollection(users);
            ReloadPlayers();
            this.IsRed = true;
            LogoutCmd = new RelayCommand(pars => Logout());
            InviteToPlayCmd = new RelayCommand(pars => InviteToPlay(pars));
            ChangeStatusCmd = new RelayCommand(pars => ChangeStatus(pars));

            Task.Run(async () =>
            {
                while (true)
                {
                    await Task.Delay(1000);
                    ReloadPlayers();
                    Console.WriteLine("kutłas refresh");
                }
            });
        }

        public void Logout()
        {
            Context.AccountService.Logout(Context.UserToken);
            Context.UserToken = null;
            Context.SetPage(new LoginPage(Context));
        }
        public void ReloadPlayers()
        {
            var users = Context.GameAdapter.GetReadyUsers().ToList();
            users.Remove(Context.UserToken.Username);

            if (!users.SequenceEqual(this.users))
            {
                this.users = users;
                Context.Frame.Dispatcher.Invoke(() =>
                {
                    Players.Clear();
                    foreach (var user in users)
                    {
                        Players.Add(user);
                    }
                });
            }
            //this.users.Clear();
            //this.users.AddRange(users);
        }
        public void ChangeStatus(object parameter)
        {
            Ellipse statusEllipse = parameter as Ellipse;
            if(IsRed)
            {
                Context.GameAdapter.WaitToGame(new OnGameOffer(Context));
                statusEllipse.Fill = Brushes.Green;
            }
            else
            {
                Context.GameAdapter.DontWaitToGame();
                statusEllipse.Fill = Brushes.Red;
            }
            IsRed = !IsRed;
        }
        public void InviteToPlay(object parameter)
        {
            string username = parameter as string;
            Context.GameAdapter.PlayWithUser(username, new OnGameEstablished(Context), new OnGameOfferRejected(Context));
        }
    }
}
