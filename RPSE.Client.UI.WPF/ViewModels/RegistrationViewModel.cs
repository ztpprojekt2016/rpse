﻿using RPSE.Client.UI.WPF.Views;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;

namespace RPSE.Client.UI.WPF.ViewModels
{
    class RegistrationViewModel
    {
        public Context Context { get; set; }
        public ICommand ToLoginCmd { get; set; }
        public ICommand RegistrationCmd { get; set; }
        public RegistrationViewModel(Context context)
        {
            this.Context = context;
            ToLoginCmd = new RelayCommand(pars => ToLogin());
            RegistrationCmd = new RelayCommand(pars => Registration(pars));
        }
        public void Registration(object parameter)
        {
            var values = (object[])parameter;
            PasswordBox passwordBox = values[0] as PasswordBox;
            PasswordBox passwordConfirmBox = values[1] as PasswordBox;
            TextBox nicknameBox = values[2] as TextBox;
            string password = passwordBox.Password;
            string confirmPassword = passwordConfirmBox.Password;
            string nickname = nicknameBox.Text;
            if(password == confirmPassword)
            {
                var reg = Context.AccountService.Register(nickname, password);
                if(reg)
                {
                    Context.SetPage(new LoginPage(Context));
                }
                else
                {
                    MessageBox.Show("Błąd");
                }
            }
            else
            {
                MessageBox.Show("Są różne");
            }
        }
        public void ToLogin()
        {
            Context.SetPage(new LoginPage(Context));
        }
    }
}
