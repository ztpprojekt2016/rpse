﻿using RPSE.Client.UI.WPF.Views;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;

namespace RPSE.Client.UI.WPF.ViewModels
{
    public class LoginViewModel
    {
        public Context Context { get; set; }
        public ICommand ToRegistrationCmd { get; set; }
        public ICommand LoginCmd { get; set; }

        public LoginViewModel(Context context)
        {
            this.Context = context;
            ToRegistrationCmd = new RelayCommand(pars => ToRegistration());
            LoginCmd = new RelayCommand(pars => Login(pars));
        }

        public void Login(object parameter)
        {
            var values = (object[])parameter;
            PasswordBox passwordBox = values[0] as PasswordBox;
            TextBox nicknameBox = values[1] as TextBox;
            string password = passwordBox.Password;
            string nickname = nicknameBox.Text;
            var log = Context.AccountService.Login(nickname, password);
            if(log != null)
            {
                Context.UserToken = log;
                Context.GameAdapter.UserToken = log;
                Context.SetPage(new MainPage(Context));
            }
            else
            {
                MessageBox.Show("Błąd logowania");
            }
        }

        public void ToRegistration()
        {
            Context.SetPage(new RegistrationPage(Context));
        }
    }
}
