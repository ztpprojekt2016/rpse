﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;

namespace RPSE.Client.UI.WPF.ViewModels
{
    class RejectViewModel
    {
        private string username;
        public String GetInfo
        {
            get
            {
                return String.Format("Gracz {0} odrzucił zaproszenie.", username);
            }
        }
        public ICommand AcceptCmd { get; set; } 

        public RejectViewModel(string username)
        {
            this.username = username;
            AcceptCmd = new RelayCommand(pars => Accept(pars));
        }
        
        public void Accept(object parameter)
        {
            Window window = parameter as Window;
            if (window != null)
            {
                window.Close();
            }
        }
    }
}
