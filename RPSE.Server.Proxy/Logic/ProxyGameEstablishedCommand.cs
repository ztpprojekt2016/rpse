﻿using RPSE.Shared.Commands;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RPSE.Shared.Auth;
using System.Net.Sockets;

namespace RPSE.Server.Proxy.Logic
{
    class ProxyGameEstablishedCommand : GameEstablishedCommand
    {
        private int id;
        Socket connection;
        TcpServer server;

        public ProxyGameEstablishedCommand(int id, Socket connection, TcpServer server)
        {
            this.id = id;
            this.connection = connection;
            this.server = server;
        }

        public override void Execute(GameToken gameToken)
        {
            byte[][] arr = new byte[3][];
            arr[0] = server.Compress("RunGameEstablishedCommand");
            arr[1] = server.Compress(this.id);
            arr[2] = server.Compress(gameToken);

            byte[] data = server.Compress(arr);
            connection.Send(data);
        }
    }
}
