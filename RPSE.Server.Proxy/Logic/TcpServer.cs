﻿using RPSE.Server.Proxy.Logic;
using RPSE.Shared.Auth;
using RPSE.Shared.Interfaces;
using RPSE.Shared.Items;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading.Tasks;

namespace RPSE.Server.Proxy.Logic
{
    class TcpServer
    {
        public const int BufferSize = 4096;

        public IGameService GameService { get; set; }
        public IAccountService AccountService { get; set; }
        private IList<Socket> sockets = new List<Socket>();
        private TcpListener tcp;
        string ip;

        public TcpServer(string ip)
        {
            this.ip = ip;
        }

        public void Init()
        {
            tcp = new TcpListener(IPAddress.Parse(ip), 6969);
            tcp.Start();
            Console.WriteLine("S: Listener started");

            Task.Run(() =>
            {
                while (true)
                {
                    Socket client = tcp.AcceptSocket();
                    Console.WriteLine("S: Connection accepted.");

                    sockets.Add(client);

                    Task.Run(() =>
                    {
                        byte[] data = new byte[BufferSize];

                        while (true)
                        {

                            int size = client.Receive(data);

                            var trimData = new byte[size];
                            Array.Copy(data, trimData, trimData.Length);
                            Recieve(trimData, client);

                            Console.WriteLine("S: Recieved data: ");


                            /*for (int i = 0; i < size; i++)
                                Console.Write(Convert.ToChar(data[i]));
                            Console.WriteLine();*/
                        }
                    });
                }
            });
        }
        public byte[] Compress(object o)
        {
            byte[] bytes;
            IFormatter formatter = new BinaryFormatter();
            using (MemoryStream stream = new MemoryStream())
            {
                formatter.Serialize(stream, o);
                bytes = stream.ToArray();
            }

            return bytes;
        }

        private Object Decompress(byte[] bytes)
        {
            object o;
            IFormatter formatter = new BinaryFormatter();
            using (MemoryStream stream = new MemoryStream(bytes))
            {
                o = formatter.Deserialize(stream);
            }

            return o;
        }

        private void Recieve(byte[] bytes, Socket client)
        {
            byte[][] deserialized = (byte[][])Decompress(bytes);
            string function = (string)Decompress(deserialized[0]);


            Console.WriteLine(function + " called");
            switch (function)
            {
                case "WaitToGame":
                    int id = (int)Decompress(deserialized[1]);
                    UserToken userToken = (UserToken)Decompress(deserialized[2]);

                    var proxyOfferCommand = new ProxyOfferCommand(id, client, this);
                    GameService.WaitToGame(userToken, proxyOfferCommand);
                    break;
                case "DontWaitToGame":
                    UserToken userToken1 = (UserToken)Decompress(deserialized[1
                        ]);
                    GameService.DontWaitToGame(userToken1);
                    break;
                case "PlayWithUser":
                    int idest = (int)Decompress(deserialized[1]);
                    int idre = (int)Decompress(deserialized[2]);
                    UserToken userToken2 = (UserToken)Decompress(deserialized[3]);
                    string username = (string)Decompress(deserialized[4]);

                    GameService.PlayWithUser(userToken2,
                        username,
                        new ProxyGameEstablishedCommand(idest, client, this),
                        new ProxyGameOfferRejectedCommand(idre, client, this));
                    break;
                case "AcceptOffer":
                    int id2 = (int)Decompress(deserialized[1]);
                    UserToken userToken3 = (UserToken)Decompress(deserialized[2]);
                    OfferToken offerToken = (OfferToken)Decompress(deserialized[3]);
                    var proxyGameEstablishedCommand = new ProxyGameEstablishedCommand(id2, client, this);
                    GameService.AcceptOffer(userToken3, offerToken, proxyGameEstablishedCommand);
                    break;
                case "RejectOffer":
                    UserToken userToken4 = (UserToken)Decompress(deserialized[1]);
                    OfferToken offerToken1 = (OfferToken)Decompress(deserialized[2]);
                    GameService.RejectOffer(userToken4, offerToken1);
                    break;
                case "Surrender":
                    UserToken userToken5 = (UserToken)Decompress(deserialized[1]);
                    GameToken gameToken = (GameToken)Decompress(deserialized[2]);
                    GameService.Surrender(userToken5, gameToken);
                    break;
                case "SelectItem":
                    UserToken userToken6 = (UserToken)Decompress(deserialized[1]);
                    GameToken gameToken1 = (GameToken)Decompress(deserialized[2]);
                    ProxyItem proxyItem = (ProxyItem)Decompress(deserialized[3]);
                    var item = proxyItem.GetItem();
                    GameService.SelectItem(userToken6, gameToken1, item);
                    break;
                case "InitGame":
                    int idst = (int)Decompress(deserialized[1]);
                    int idro = (int)Decompress(deserialized[2]);
                    int idgo = (int)Decompress(deserialized[3]);
                    UserToken userToken7 = (UserToken)Decompress(deserialized[4]);
                    GameToken gameToken2 = (GameToken)Decompress(deserialized[5]);

                    GameService.InitGame(
                        userToken7,
                        gameToken2,
                        new ProxyRoundStartCommand(idst, client, this),
                        new ProxyRoundOverCommand(idro, client, this),
                        new ProxyGameOverCommand(idgo, client, this)
                        );
                    break;
                case "Login":
                    string username_ = (string)Decompress(deserialized[1]);
                    string password_ = (string)Decompress(deserialized[2]);

                    var result = AccountService.Login(username_, password_);

                    byte[][] arr = new byte[4][];
                    arr[0] = Compress("LoginResult");
                    if (result == null)
                    {
                        arr[1] = Compress("null");
                    }
                    else
                    {
                        arr[1] = Compress("ok");
                        arr[2] = Compress(result);
                    }
                    
                    byte[] data = Compress(arr);
                    client.Send(data);

                    break;
                case "Register":
                    string username__ = (string)Decompress(deserialized[1]);
                    string password__ = (string)Decompress(deserialized[2]);

                    var result_ = AccountService.Register(username__, password__);

                    byte[][] arr_ = new byte[4][];
                    arr_[0] = Compress("RegisterResult");
                    arr_[1] = Compress(result_);

                    byte[] data_ = Compress(arr_);
                    client.Send(data_);

                    break;
                case "Logout":
                    UserToken userToken_ = (UserToken)Decompress(deserialized[1]);

                    AccountService.Logout(userToken_);
                    break;
                case "GetReadyUsers":
                    IList<string> result___ = GameService.GetReadyUsers();

                    byte[][] arr__ = new byte[4][];
                    arr__[0] = Compress("GetReadyUsersResult");
                    arr__[1] = Compress(result___);

                    byte[] data__ = Compress(arr__);
                    client.Send(data__);

                    break;
                default:
                    Console.WriteLine("Default case");
                    break;
            }
        }

    }
}
