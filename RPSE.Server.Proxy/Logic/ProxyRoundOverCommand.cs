﻿using RPSE.Shared.Commands;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RPSE.Shared.Conteners;
using System.Net.Sockets;
using RPSE.Shared.Items;

namespace RPSE.Server.Proxy.Logic
{
    class ProxyRoundOverCommand : RoundOverCommand
    {
        private int id;
        Socket connection;
        TcpServer server;

        public ProxyRoundOverCommand(int id, Socket connection, TcpServer server)
        {
            this.id = id;
            this.connection = connection;
            this.server = server;
        }

        public override void Execute(RoundOverInfo roundInfo)
        {
            var keys = new List<string>(roundInfo.Choices.Keys);
            foreach (string key in keys)
            {
                roundInfo.Choices[key] = new ProxyItem(roundInfo.Choices[key]);
            }

            byte[][] arr = new byte[3][];
            arr[0] = server.Compress("RunProxyRoundOverCommand");
            arr[1] = server.Compress(this.id);
            arr[2] = server.Compress(roundInfo);

            byte[] data = server.Compress(arr);
            connection.Send(data);
        }
    }
}
