﻿using RPSE.Shared.Commands;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RPSE.Shared.Conteners;
using System.Net.Sockets;

namespace RPSE.Server.Proxy.Logic
{
    class ProxyGameOverCommand : GameOverCommand
    {
        private int id;
        Socket connection;
        TcpServer server;

        public ProxyGameOverCommand(int id, Socket connection, TcpServer server)
        {
            this.id = id;
            this.connection = connection;
            this.server = server;
        }

        public override void Execute(GameOverInfo gameInfo)
        {
            byte[][] arr = new byte[3][];
            arr[0] = server.Compress("RunGameOverCommand");
            arr[1] = server.Compress(this.id);
            arr[2] = server.Compress(gameInfo);

            byte[] data = server.Compress(arr);
            connection.Send(data);
        }
    }
}
