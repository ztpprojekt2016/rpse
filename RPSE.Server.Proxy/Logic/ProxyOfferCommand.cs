﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RPSE.Shared.Auth;
using RPSE.Shared.Commands;
using RPSE.Shared.Conteners;
using System.Net.Sockets;
using RPSE.Server.Core.Services;

namespace RPSE.Server.Proxy.Logic
{
    class ProxyOfferCommand : GameOfferCommand
    {
        private int id;
        Socket connection;
        TcpServer server;

        public ProxyOfferCommand(int id , Socket connection, TcpServer server)
        {
            this.id = id;
            this.connection = connection;
            this.server = server;
        }

        public override void Execute(OfferToken offerToken, OfferInfo offerInfo)
        {
            byte[][] arr = new byte[4][];
            arr[0] = server.Compress("RunOfferCommand");
            arr[1] = server.Compress(this.id);
            arr[2] = server.Compress(offerToken);
            arr[3] = server.Compress(offerInfo);

            byte[] data = server.Compress(arr);
            connection.Send(data);
        }
    }
}

