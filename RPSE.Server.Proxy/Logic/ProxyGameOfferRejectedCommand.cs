﻿using RPSE.Shared.Commands;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;

namespace RPSE.Server.Proxy.Logic
{
    class ProxyGameOfferRejectedCommand : GameOfferRejectedCommand
    {
        private int id;
        Socket connection;
        TcpServer server;

        public ProxyGameOfferRejectedCommand(int id, Socket connection, TcpServer server)
        {
            this.id = id;
            this.connection = connection;
            this.server = server;
        }

        public override void Execute(string username)
        {
            byte[][] arr = new byte[3][];
            arr[0] = server.Compress("RunGameOfferRejectedCommand");
            arr[1] = server.Compress(this.id);
            arr[2] = server.Compress(username);

            byte[] data = server.Compress(arr);
            connection.Send(data);
        }
    }
}
