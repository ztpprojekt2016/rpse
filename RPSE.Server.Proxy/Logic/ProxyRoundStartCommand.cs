﻿using RPSE.Shared.Commands;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RPSE.Shared.Conteners;
using System.Net.Sockets;
using RPSE.Shared.Items;

namespace RPSE.Server.Proxy.Logic
{
    class ProxyRoundStartCommand : RoundStartCommand
    {
        private int id;
        Socket connection;
        TcpServer server;

        public ProxyRoundStartCommand(int id, Socket connection, TcpServer server)
        {
            this.id = id;
            this.connection = connection;
            this.server = server;
        }

        public override void Execute(RoundInfo roundInfo)
        {
            var items = roundInfo.Items;
            var proxyItems = new List<ProxyItem>();
            foreach (var item in items)
            {
                proxyItems.Add(new ProxyItem(item));
            }

            roundInfo.Items = proxyItems.ToArray();

            byte[][] arr = new byte[3][];
            arr[0] = server.Compress("RunProxyRoundStartCommand");
            arr[1] = server.Compress(this.id);
            arr[2] = server.Compress(roundInfo);

            byte[] data = server.Compress(arr);
            connection.Send(data);
        }
    }
}
