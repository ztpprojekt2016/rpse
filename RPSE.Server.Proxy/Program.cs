﻿using RPSE.Server.Core.Services;
using RPSE.Server.Proxy.Logic;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPSE.Server.Proxy
{
    public class Program
    {
        public static void Main(string[] args)
        {
            try
            {
                Server.BotConsole.Program.Main(args);

                var gameService = new GameService();
                var accountService = new AccountService();

                string ip = "";
                try
                {
                    using (StreamReader reader = new StreamReader("ip.txt"))
                    {
                        ip = reader.ReadLine();
                    }
                }
                catch (Exception)
                {
                    Console.WriteLine("nie znajduję pliku ip.txt :/");
                    Console.ReadLine();
                    Environment.Exit(0);
                }

                var tcpServer = new TcpServer(ip);
                tcpServer.AccountService = accountService;
                tcpServer.GameService = gameService;

                tcpServer.Init();

                while (true)
                {
                    if (Console.ReadLine() == "exit")
                    {
                        break;
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                Console.ReadLine();
            }
        }
    }
}
