﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using RPSE.Client.Libs;
using RPSE.Client.Libs.Adapter;
using RPSE.Client.Libs.Proxy;
using RPSE.Shared.Auth;
using RPSE.Shared.Commands;
using RPSE.Shared.Conteners;
using RPSE.Shared.Interfaces;
using System.IO;

namespace RPSE.Client.Demo
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        [DllImport("Kernel32")]
        public static extern void AllocConsole();

        [DllImport("Kernel32")]
        public static extern void FreeConsole();


        private GameServiceProxy gameService = new GameServiceProxy();
        public GameAdapter GameAdapter { get; set; }

        public List<GameWindow> GameWindows { get; set; } = new List<GameWindow>();
        public UserToken UserToken { get; set; }
        AccountServiceProxy accountService = new AccountServiceProxy();

        private void button_Clck(object sender, RoutedEventArgs e)
        {
            UserToken = accountService.Login("login", "pass");
        }

        public MainWindow()
        {
            InitializeComponent();
            AllocConsole(); //temp

            GameAdapter = new GameAdapter(gameService);


            string ip = "";
            try
            {
                using (StreamReader reader = new StreamReader("ip.txt"))
                {
                    ip = reader.ReadLine();
                }
            }
            catch (Exception)
            {
                MessageBox.Show("nie znajduję pliku ip.txt :/");
                Environment.Exit(0);
            }
            

            TcpClient tcpClient = new TcpClient(ip);
            gameService.TcpClient = tcpClient;
            tcpClient.GameServiceProxy = gameService;
            accountService.TcpClient = tcpClient;
            tcpClient.AccountServiceProxy = accountService;

            try
            {
                tcpClient.Init();
            }
            catch (Exception ex)
            {
                MessageBox.Show("ssij, serwer nie działa");
                Environment.Exit(0);
                //Application.Current.Shutdown();
            }

            Console.WriteLine("Hi!");
        }

        private void button_Click(object sender, RoutedEventArgs e)
        {
            var reg = accountService.Register("aaa", "aaa");
            Console.WriteLine(reg ? "register ok" : "register error");
        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            var users = GameAdapter.GetReadyUsers();
            UsersList.Items.Clear();

            Console.WriteLine("Users ready to game:");
            foreach (var user in users)
            {
                UsersList.Items.Add(user);
                Console.WriteLine("  " + user);
            }
        }

        private void Button_Click_2(object sender, RoutedEventArgs e)
        {
            accountService.Logout(UserToken);
            Console.WriteLine("Logout");
        }

        private void Button_Click_3(object sender, RoutedEventArgs e)
        {
            GameAdapter.PlayWithUser("Bot", new OnGameEstablished(this), new OnGameOfferRejected());
        }



        class OnGameEstablished : GameEstablishedCommand
        {
            private MainWindow mainWindow;

            public OnGameEstablished(MainWindow mainWindow)
            {
                this.mainWindow = mainWindow;
            }

            public override void Execute(GameToken gameToken)
            {
                mainWindow.Dispatcher.Invoke(() =>
                {
                    var w = new GameWindow();
                    w.GameToken = gameToken;
                    w.GameAdapter = mainWindow.GameAdapter;
                    w.UserToken = mainWindow.UserToken;
                    w.Init();

                    mainWindow.GameWindows.Add(w);
                    w.Show();
                });
            }
        }

        class OnGameOfferRejected : GameOfferRejectedCommand
        {
            public override void Execute(string username)
            {
                MessageBox.Show(username + " odrzucił twoje zaproszenie do gry");
            }
        }

        class OnGameOfferCommand : GameOfferCommand
        {
            private MainWindow mainWindow;

            public OnGameOfferCommand(MainWindow mainWindow)
            {
                this.mainWindow = mainWindow;
            }

            public override void Execute(OfferToken offerToken, OfferInfo offerInfo)
            {
                mainWindow.Dispatcher.Invoke(() => {
                    MessageBox.Show("ktos cie zaprosil parowo");
                });
            }
        }

        private void Button_Click_4(object sender, RoutedEventArgs e)
        {
            var log = accountService.Login("aaa", "aaa");
            GameAdapter.UserToken = log;
            Console.WriteLine("Login: " + (log != null ? "login ok" : "login error"));
            UserToken = log;
        }

        private void button_Click_5(object sender, RoutedEventArgs e)
        {
            if (UsersList.SelectedItem == null)
                return;

            GameAdapter.PlayWithUser((string)UsersList.SelectedItem, new OnGameEstablished(this), new OnGameOfferRejected());
        }



        private void button1_Click(object sender, RoutedEventArgs e)
        {
            GameAdapter.WaitToGame(new OnGameOfferCommand(this));
        }

        private void button2_Click(object sender, RoutedEventArgs e)
        {
            var reg = accountService.Register("bbb", "bbb");
            Console.WriteLine(reg ? "register ok" : "register error");
        }

        private void button3_Click(object sender, RoutedEventArgs e)
        {
            var log = accountService.Login("bbb", "bbb");
            GameAdapter.UserToken = log;
            Console.WriteLine("Login: " + (log != null ? "login ok" : "login error"));
            UserToken = log;
        }
    }
}
