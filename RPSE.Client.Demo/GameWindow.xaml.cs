﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using RPSE.Client.Libs.Adapter;
using RPSE.Shared.Auth;
using RPSE.Shared.Commands;
using RPSE.Shared.Conteners;
using RPSE.Shared.Items;

namespace RPSE.Client.Demo
{
    /// <summary>
    /// Interaction logic for GameWindow.xaml
    /// </summary>
    public partial class GameWindow : Window
    {
            public GameToken GameToken { get; set; }
            public GameAdapter GameAdapter { get; internal set; }
            public UserToken UserToken { get; internal set; }

            public GameWindow()
            {
                InitializeComponent();
            }

            public void Init()
            {
                GameAdapter.InitGame(GameToken, new OnRoundStart(this), new OnRoundOver(this), new OnGameOver(this));
                SetGrid(Grids.None);
            }


            public enum Grids
            {
                None,
                Round,
                RoundOver,
                GameOver
            }

            public void SetGrid(Grids grid)
            {
                NoneGrid.Visibility = Visibility.Hidden;
                RoundGrid.Visibility = Visibility.Hidden;
                RoundOverGrid.Visibility = Visibility.Hidden;
                GameOverGrid.Visibility = Visibility.Hidden;

                switch (grid)
                {
                    case Grids.None:
                        NoneGrid.Visibility = Visibility.Visible;
                        break;
                    case Grids.Round:
                        RoundGrid.Visibility = Visibility.Visible;
                        break;
                    case Grids.RoundOver:
                        RoundOverGrid.Visibility = Visibility.Visible;
                        break;
                    case Grids.GameOver:
                        GameOverGrid.Visibility = Visibility.Visible;
                        break;
                }
            }




            class OnRoundStart : RoundStartCommand
            {
                private GameWindow gameWindow;

                public OnRoundStart(GameWindow gameWindow)
                {
                    this.gameWindow = gameWindow;
                }

                public override void Execute(RoundInfo roundInfo)
                {
                    gameWindow.Dispatcher.Invoke(() =>
                    {
                        gameWindow.RoundGridPlayers.Content = "Gracze: " + String.Join(", ", roundInfo.Users);
                        gameWindow.RoundGridRound.Content = "Runda: " + roundInfo.Round;
                        gameWindow.RoundGridItems.Items.Clear();
                        foreach (var item in roundInfo.Items)
                            gameWindow.RoundGridItems.Items.Add(item);

                        gameWindow.SetGrid(Grids.Round);
                    });
                }
            }

            class OnRoundOver : RoundOverCommand
            {
                private GameWindow gameWindow;

                public OnRoundOver(GameWindow gameWindow)
                {
                    this.gameWindow = gameWindow;
                }

                public override void Execute(RoundOverInfo roundInfo)
                {
                    gameWindow.Dispatcher.Invoke(() =>
                    {
                        gameWindow.RoundOverGridPoints.Content = "Zdobyte punkty: " + roundInfo.Points;
                        gameWindow.RoundOverGridResult.Content = roundInfo.Result;
                        gameWindow.RoundOverGridChoices.Content =
                            String.Join("\n", roundInfo.Choices.Select(x => x.Key + " : " + x.Value.Name));

                        gameWindow.SetGrid(Grids.RoundOver);
                    });
                }
            }

            class OnGameOver : GameOverCommand
            {
                private GameWindow gameWindow;

                public OnGameOver(GameWindow gameWindow)
                {
                    this.gameWindow = gameWindow;
                }

                public override void Execute(GameOverInfo gameInfo)
                {
                    gameWindow.Dispatcher.Invoke(() =>
                    {
                        gameWindow.GameOverGridPoints.Content = "zdobyte punkty w sumie: " + gameInfo.Points;
                        gameWindow.GameOverGridResult.Content = "ostateczny rezultat: " + gameInfo.Result;

                        gameWindow.SetGrid(Grids.GameOver);
                    });
                }
            }

            private void RoundGridItems_SelectionChanged(object sender, SelectionChangedEventArgs e)
            {
                if (RoundGridItems.SelectedItem == null)
                    return;

                var item = (Item)RoundGridItems.SelectedItem;

                RoundGridItem.Content = item.Name;
            }

            private void Button_Click(object sender, RoutedEventArgs e)
            {
                if (RoundGridItems.SelectedItem == null)
                {
                    MessageBox.Show("Wybierz coś");
                    return;
                }
                var item = (Item)RoundGridItems.SelectedItem;

                GameAdapter.SelectItem(GameToken, item);
            }

            private void button_Click_1(object sender, RoutedEventArgs e)
            {
                Close();
            }
        }
}
